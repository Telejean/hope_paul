const Sequelize = require("sequelize");

//PRODUCTIE

// const sequelize = new Sequelize(
//   "siscro_academie_2022",
//   "academie_2022",
//   "^nzaHG2s[}UW}CU%",
//   {
//     dialect: "mysql",
//     host: "localhost",
//     dialectOptions: {
//       charset: "utf8",
//       collate: "utf8_general_ci",
//     },
//     define: {
//       timestamps: true,
//     },
//   }
// );

//LOCAL

const sequelize = new Sequelize("siscro_academie_2023", "root", "", {
  dialect: "mysql",
  host: "localhost",
  define: {
    timestamps: true,
  },
});

module.exports = sequelize;
