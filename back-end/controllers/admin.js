const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;

const AdminDb = require("../models").Admin;

const controller = {
    getAdminByEmail: async (req, res) => {
        try {
            const email = req.params.email;
            let admin = await AdminDb.findOne({
                where: {
                    email: email
                }
            });
            if (!admin.length) throw new Error("gol");
            res.status(200).send(admin);
        } catch (err) {
            if (err.message === "gol") res.status(404).send("Nu exista admin!");
            else res.status(500).send({ message: "Server error!" });
        }
    },

    createAdmin: async (req, res) => {
        try {
            const admin = {
                email: req.body.email
            };
           
                if (!admin.email) {
                    throw new Error("email lipsa");
                }
                
                if(admin.email.match(`^[\w.+\-]+@gmail\.com$`))
                    {
                     throw new Error("email invalid")
                    }

            let newAdmin = await AdminDb.create(admin);
            res.status(201).send({newAdmin,message:"Succes"});
        } catch (err) {
          if(err.message) res.status.send(400).json({message:`${err.message}`})
          else {
            res.status.send(500).json({message:"Erore de server"})
          }
        }
    },

}

module.exports = controller;
