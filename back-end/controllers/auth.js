const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const AdminDb = require("../models").Admin;
require("dotenv").config();

passport.serializeUser((admin, done) => {
    console.log({admin:admin});
    done(null, admin.id);
})

passport.deserializeUser(async (id, done) => {
    console.log({id:id});
    const admin = await AdminDb.findOne({ where: { id: id } })
    done(null, admin.get());
})

passport.use(
    new GoogleStrategy(
        {
            clientID: "54049349682-v1g39gpttk976ul4ip28h17dl6h60h4q.apps.googleusercontent.com",
            clientSecret: "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",
            callbackURL: "http://localhost:7070/api/auth/google/redirect",
        },
        (accessToken, refreshToken, profile, done) => {
            AdminDb.findOne({
                where: {
                    email: profile.emails[0].value,
                },
            }).then((currentAdmin) => {
                if (currentAdmin == null) {
                    done(false, null);
                } else {
                    done(null, currentAdmin.get());
                }
            });
        }
    )
)