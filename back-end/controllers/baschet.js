const BaschetDb = require("../models").Baschet;

const controller = {
  getAllEchipeBaschet: async (req, res) => {
    try {
      let echipeBaschet = await BaschetDb.findAll({ paranoid: false });
      if (!echipeBaschet.length) throw new Error("gol");
      res.status(200).send(echipeBaschet);
    } catch (err) {
      if (err.message === "gol")
        res.status(404).send("Nu exista echipe de baschet!");
      else res.status(500).send({ message: "Server error!" });
    }
  },

  createEchipaBaschet: (req, res) => {
    const echipaBaschet = {
      numeEchipa: req.body.numeEchipa,
      numeCapitan: req.body.numeCapitan,
      emailCapitan: req.body.emailCapitan,
      telefonCapitan: req.body.telefonCapitan,
      studiiCapitan: req.body.studiiCapitan,
      numeCoechipieri: req.body.numeCoechipieri,
      undeAflat: req.body.undeAflat,
      aiVreaSaSpui: req.body.aiVreaSaSpui,
    };

    let error = [];
    let dateCompletate = Object.values(echipaBaschet);
    let dateDeCompletat = Object.keys(echipaBaschet);
    for (let camp = 0; camp < dateCompletate.length; camp++) {
      if (dateCompletate[camp] === undefined || dateCompletate[camp] === "") {
        error.push(`undefined ${dateCompletate[camp]}`);
        console.log(`Nu a fost completat campul ${dateCompletate[camp]}`);
      } else {
        if (dateDeCompletat[camp] == "numeEchipa") {
          if (echipaBaschet.numeEchipa.length > 100) {
            error.push(`Numele echipei poate contine maxim 100 de caractere`);
            console.log(`Numele echipei poate contine maxim 100 de caractere`);
          } else if (
            !echipaBaschet.numeEchipa.match(`^[A-Za-z0-9ăîșțâĂÎȘȚÂ \s]+$`)
          ) {
            error.push(`Numele echipei poate contine doar litere si cifre`);
            console.log(`Numele echipei poate contine doar litere si cifre`);
          }
        } else if (dateDeCompletat[camp] == "numeCapitan") {
          if (echipaBaschet.numeCapitan.length > 50) {
            error.push(`Numele capitanului este prea lung`);
            console.log(`Numele capitanului este prea lung`);
          } else if (
            !echipaBaschet.numeCapitan.match(`^[A-Za-zăîșțâĂÎȘȚÂ \s]+$`)
          ) {
            error.push(`Numele capitanului poate contine doar litere`);
            console.log(`Numele capitanului poate contine doar litere`);
          }
        } else if (dateDeCompletat[camp] == "emailCapitan") {
          if (
            !echipaBaschet.emailCapitan.includes("@gmail.com") &&
            !echipaBaschet.emailCapitan.includes("@yahoo.com") &&
            !echipaBaschet.emailCapitan.includes("@hotmail.com")
          ) {
            error.push(`Emailul introdus este invalid`);
            console.log(`Emailul introdus este invalid`);
          }
        } else if (dateDeCompletat[camp] == "telefonCapitan") {
          if (echipaBaschet.telefonCapitan.length !== 10) {
            console.log(`Numarul de telefon este invalid1`);
            error.push(`Numarul de telefon este invalid1`);
          } else {
            if (
              !echipaBaschet.telefonCapitan.substring(0, 2).includes(`07`) && !echipaBaschet.telefonCapitan.substring(2).match(`^[0-9]+$`)
            ) {
              error.push(`Numarul de telefon este invalid2`);
              console.log(`Numarul de telefon este invalid2`);
            }
          }
        }
      }

      if (dateCompletate[camp] == "numeCoechipieri")
      {if (echipaBaschet.numeCoechipieri.length > 150) {
        console.log(
          'Campul "Numele coechipierilor" poate contine maxim 150 de caractere!'
        );
        error.push(
          'Campul "Numele coechipierilor" poate contine maxim 150 de caractere!'
        );
      }
      if (
        !echipaBaschet.numeCoechipieri.match(`^[A-Za-zăîșțâĂÎȘȚÂ, \s ]+$`)
      ) {
        error.push("Numele Coechipierilor trebuie sa contina doar litere");
        console.log("Numele Coechipierilor trebuie sa contina doar litere");
      }
    }
        
        
      if (dateCompletate[camp] == "undeAflat")
        if (echipaBaschet.undeAflat.length > 500) {
          error.push("Campul poate contine maxim 500 de caractere!");
          console.log("Campul poate contine maxim 500 de caractere!");
        }
      if (
        echipaBaschet.acordDatePersonale === false ||
        echipaBaschet.acordRegulament === false
      ) {
        error.push("Va rugam sa bifati termenii si conditiile!");
        console.log("Va rugam sa bifati termenii si conditiile!");
      }
    }

    if (error.length === 0) {
      BaschetDb.create(echipaBaschet)
        .then((newEchipa) => {
          res.status(201).send(newEchipa);
        })
    } else {
     // res.status(500).send({ message: "Server error!" });
     
      console.log(err);
      res.status(400).send({message: "Unul sau mai multe campuri nu au fost completate!"})
    }
  },

  updateParticipaBaschet: async (req, res) => {
    try {
      const id = req.params.id;
      let entry = await BaschetDb.findOne({
        where: {
          baschetId: id,
        },
      });
      if (entry) {
        await entry.update({ participa: !entry.participa });
        entry = await entry.save();
        res.status(200).send(entry);
      } else {
        res.status(404).send("Nu exista inregistrare cu id-ul cerut");
      }
    } catch (err) {
      res.status(500).send("Server error!");
      console.log(err.message);
    }
  },

  deleteBaschetById: async (req, res) => {
    try {
      const id = req.params.id;
      let entry = await BaschetDb.findOne({
        where: {
          baschetId: id,
        },
      });
      console.log(JSON.stringify(entry));
      if (entry) {
        await entry.destroy();
        res.status(200).send("Echipa stearsa!");
      } else {
        res.status(404).send("Nu exista inregistrare cu email-ul cerut");
      }
    } catch (err) {
      res.status(500).send("Server error!");
      console.log(err.message);
    }
  },

  recoverEchipaBaschet: async (req, res) => {
    try {
      const id = req.params.id;
      let echipaBaschet = await BaschetDb.findOne({
        paranoid: false,
        where: {
          baschetId: id,
        },
      });
      if (echipaBaschet) {
        echipaBaschet.restore();
        res.status(200).send(echipaBaschet);
      } else throw new Error("gol");
    } catch (err) {
      if (err.message === "gol")
        res.status(404).send("Nu exista echipa de baschet!");
      else res.status(500).send({ message: "Server error!" });
    }
  },
};

module.exports = controller;
