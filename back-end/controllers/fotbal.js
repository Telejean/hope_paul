const FotbalDb = require("../models").Fotbal;

const controller = {
  getAllEchipeFotbal: async (req, res) => {
    try {
      let echipeFotbal = await FotbalDb.findAll({ paranoid: false });
      if (!echipeFotbal.length) throw new Error("gol");
      res.status(200).send(echipeFotbal);
    } catch (err) {
      if (err.message === "gol")
        res.status(404).send("Nu exista echipe de fotbal!");
      else res.status(500).send({ message: "Server error!" });
    }
  },

  createEchipaFotbal: (req, res) => {
    const echipaFotbal = {
      numeEchipa: req.body.numeEchipa,
      studiiCapitan: req.body.studiiCapitan,
      numeCapitan: req.body.numeCapitan,
      emailCapitan: req.body.emailCapitan,
      telefonCapitan: req.body.telefonCapitan,
      numeCoechipieri: req.body.numeCoechipieri,
      undeAflat: req.body.undeAflat,
      aiVreaSaSpui: req.body.aiVreaSaSpui,
    };

    let error = [];
    let echipaFotbalArr = Object.keys(echipaFotbal);
    let echipaFotbalVal = Object.values(echipaFotbal);
    let echipaFotbalLength = echipaFotbalArr.length; 
    for (let i = 0; i < echipaFotbalLength; i++) {
      if (!echipaFotbalVal[i]) {
        error.push(`Undefined ${echipaFotbal[i]}`);
        console.log(`Undefined ${echipaFotbal[i]}`);
      } else {
        if (echipaFotbalArr[i] === "numeEchipa") {
          if (
            echipaFotbal.numeEchipa.length < 2 &&
            echipaFotbal.numeEchipa.length > 50
          ) {
            error.push(
              "Numele echipei trebuie sa contina intre 2 si 50 caractere"
            );
            console.log(
              "Numele echipei trebuie sa contina intre 2 si 50 caractere"
            );
          } else if (
            !echipaFotbal.numeEchipa.match(`^[A-Za-z0-9ăîșțâĂÎȘȚÂ \s]+$`)
          ) {
            error.push("Numele echipei trebuie sa contina litere sau cifre");
            console.log("Numele echipei trebuie sa contina litere sau cifre");
          }
        }

        if (echipaFotbalArr[i] === "numeCapitan") {
          if (
            echipaFotbal.numeCapitan.length < 2 &&
            echipaFotbal.numeCapitan.length > 50
          ) {
            error.push(
              "Numele capitanului trebuie sa contina intre 2 si 50 caractere"
            );
            console.log(
              "Numele capitanului trebuie sa contina intre 2 si 50 caractere"
            );
          } else if (!echipaFotbal.numeCapitan.match(`^[A-Za-zăîșțâĂÎȘȚÂ \s]+$`)) {
            error.push("Numele capitanului trebuie sa contina doar litere");
            console.log("Numele capitanului trebuie sa contina doar litere");
          }
        }

        if (echipaFotbalArr[i] === "emailCapitan") {
          if (
            !echipaFotbal.emailCapitan.includes("@gmail.com") &&
            !echipaFotbal.emailCapitan.includes("@yahoo.com") &&
            !echipaFotbal.emailCapitan.includes("@hotmail.com")
          ) {
            error.push("Email invalid");
            console.log("Email invalid");
          }
        }

        if (echipaFotbalArr[i] === "telefonCapitan") {
          if (
            echipaFotbal.telefonCapitan.length !== 10) {
            error.push("Numarul de telefon trebuie sa contina 10 cifre");
            console.log("Numarul de telefon trebuie sa contina 10 cifre");
          } else if (
            !echipaFotbal.telefonCapitan.substring(0, 2).includes(`07`) &&
            !echipaFotbal.telefonCapitan.substring(2).match(`^[0-9]+$`)
          ) {
            error.push("Numar de telefon invalid");
            console.log("Numar de telefon invalid");
          } 
        } else if (echipaFotbalArr[i] === "numeCoechipieri") {
          if (
            echipaFotbal.numeCoechipieri.length < 2 &&
            echipaFotbal.numeCoechipieri.length > 400
          ) {
            error.push("Numele Coechipierilor este invalid");
            console.log("Numele Coechipierilor este invalid");
          } else if (
            !echipaFotbal.numeCoechipieri.match(`^[A-Za-zăîșțâĂÎȘȚÂ, \s ]+$`)
          ) {
            error.push("Numele Coechipierilor trebuie sa contina doar litere");
            console.log("Numele Coechipierilor trebuie sa contina doar litere");
          }
        } else if (echipaFotbalArr[i] === "undeAflat") {
          if (
            echipaFotbal.undeAflat.length < 2 &&
            echipaFotbal.undeAflat.length > 50
          ) {
            error.push("Ati depasit limita de cuvinte!");
            console.log("Ati depasit limita de cuvinte!");
          }
        }
        if(echipaFotbal.acordDatePersonale===false || echipaFotbal.acordRegulament===false )
        {
          error.push("Va rugam sa bifati termenii si conditiile!");
          console.log("Va rugam sa bifati termenii si conditiile!");
        }
      }
    }

    if (error.length === 0) {
      FotbalDb.create(echipaFotbal)
        .then((newEchipa) => {
          res.status(201).send(newEchipa);
        })
    } else {
      res.status(400).send({ message: "Ceva nu a mers bine!" });
    }
  },

  updateParticipaFotbal: async (req, res) => {
    try {
      const id = req.params.id;
      let entry = await FotbalDb.findOne({
        where: {
          fotbalId: id,
        },
      });
      if (entry) {
        await entry.update({ participa: !entry.participa });
        entry = await entry.save();
        res.status(200).send(entry);
      } else {
        res.status(404).send("Nu exista inregistrare cu id-ul cerut");
      }
    } catch (err) {
      res.status(500).send("Server error!");
      console.log(err.message);
    }
  },

  deleteFotbalById: async (req, res) => {
    try {
      const id = req.params.id;
      let entry = await FotbalDb.findOne({
        where: {
          fotbalId: id,
        },
      });
      console.log(JSON.stringify(entry));
      if (entry) {
        await entry.destroy();
        res.status(200).send("Echipa stearsa!");
      } else {
        res.status(404).send("Nu exista inregistrare cu email-ul cerut");
      }
    } catch (err) {
      res.status(500).send("Server error!");
      console.log(err.message);
    }
  },
  recoverEchipaFotbal: async (req, res) => {
    try {
      const id = req.params.id;
      let echipaFotbal = await FotbalDb.findOne({
        paranoid: false,
        where: {
          fotbalId: id,
        },
      });
      if (echipaFotbal) {
        echipaFotbal.restore();
        res.status(200).send(echipaFotbal);
      } else throw new Error("gol");
    } catch (err) {
      if (err.message === "gol")
        res.status(404).send("Nu exista echipa de fotbal!");
      else res.status(500).send({ message: "Server error!" });
    }
  },
};

module.exports = controller;
