const CSGODB = require('../models').CSGO;

const controller = {

       getAllEchipe: (req, res) => {
        CSGODB.findAll()
        .then((echipe) => {
            res.status(200).send(echipe);
        })
        .catch((err) => {
            console.log(err);
            res.status(200).send({message: 'Server error!'});
        })
       },
       
       addEchipa: (req, res) => {
        const echipaCSGO = {
            numeEchipa: req.body.numeEchipa,
            persoanaContact: req.body.persoanaContact,
            email: req.body.email,
            numeJucatori: req.body.numeJucatori,
            nivelStudii: req.body.nivelStudii,
            telefon: req.body.telefon,
            undeAflat: req.body.undeAflat,
            aiVreaSaSpui: req.body.aiVreaSaSpui,
        }
        let error = [];
        let echipaCSGOVal = Object.values(echipaCSGO);
        let echipaCSGOArr = Object.keys(echipaCSGO);
        for (let camp = 0; camp < echipaCSGOArr.length; camp++) {
          if (echipaCSGOArr[camp] === undefined || echipaCSGOArr[camp] === "") {
            error.push(`undefined ${echipaCSGOArr[camp]}`);
            console.log(`Nu a fost completat campul ${echipaCSGOArr[camp]}`);
          } else {
            if (echipaCSGOArr[camp] == "numeEchipa") {
              if (echipaCSGO.numeEchipa.length > 100) {
                error.push(`Numele echipei poate contine maxim 100 de caractere`);
                console.log(`Numele echipei poate contine maxim 100 de caractere`);
              } else if (
                !echipaCSGO.numeEchipa.match(`^[A-Za-z0-9ăîșțâĂÎȘȚÂ \s]+$`)
              ) {
                error.push(`Numele echipei poate contine doar litere si cifre`);
                console.log(`Numele echipei poate contine doar litere si cifre`);
              }
            } else if (echipaCSGOArr[camp] == "persoanaContact") {
              if (echipaCSGO.persoanaContact.length > 50) {
                error.push(`Numele persoanei de contact este prea lung`);
                console.log(`Numele persoanei de contact este prea lung`);
              } else if (
                !echipaCSGO.persoanaContact.match(`^[A-Za-zăîșțâĂÎȘȚÂ \s]+$`)
              ) {
                error.push(`Numele persoanei de contact poate contine doar litere`);
                console.log(`Numele persoanei de contact poate contine doar litere`);
              }
            } else if (echipaCSGOArr[camp] == "email") {
              if (
                !echipaCSGO.email.includes("@gmail.com") &&
                !echipaCSGO.email.includes("@yahoo.com") &&
                !echipaCSGO.email.includes("@hotmail.com")
              ) {
                error.push(`Emailul introdus este invalid`);
                console.log(`Emailul introdus este invalid`);
              }
            } else if (echipaCSGOArr[camp] == "telefon") {
              if (echipaCSGO.telefon.length !== 10) {
                console.log(`Numarul de telefon este invalid1`);
                error.push(`Numarul de telefon este invalid1`);
              } else {
                if (
                  !echipaCSGO.telefon.substring(0, 2).includes(`07`) && !echipaCSGO.telefon.substring(2).match(`^[0-9]+$`)
                ) {
                  error.push(`Numarul de telefon este invalid2`);
                  console.log(`Numarul de telefon este invalid2`);
                }
              }
            }
          }
    
          if (echipaCSGOArr[camp] == "numeJucatori")
          {if (echipaCSGO.numeJucatori.length > 150) {
            console.log(
              'Campul "Numele jucatorilor" poate contine maxim 150 de caractere!'
            );
            error.push(
              'Campul "Numele jucatorilor" poate contine maxim 150 de caractere!'
            );
          }
          if (
            !echipaCSGO.numeJucatori.match(`^[A-Za-zăîșțâĂÎȘȚÂ, \s ]+$`)
          ) {
            error.push("Numele jucatorilor trebuie sa contina doar litere");
            console.log("Numele jucatorilor trebuie sa contina doar litere");
          }
        }
            
            
          if (echipaCSGOArr[camp] == "undeAflat")
            if (echipaCSGO.undeAflat.length > 500) {
              error.push("Campul poate contine maxim 500 de caractere!");
              console.log("Campul poate contine maxim 500 de caractere!");
            }
        }
    
        if (error.length === 0) {
          CSGODB.create(echipaCSGO)
            .then((newEchipa) => {
              res.status(201).send(newEchipa);
            })
        } else {
         // res.status(500).send({ message: "Server error!" });
         
          console.log(err);
          res.status(400).send({message: "Unul sau mai multe campuri nu au fost completate!"})
        }
      },
       
       updateEchipaById: (req, res) =>{
            const { id } = req.params;
            if (!id) {
                res.status(400).send({ message: "Trebuie sa specifici id-ul!"});
            }

            const { numeEchipa, persoanaContact, email, numeJucatori, nivelStudii, telefon, undeAflat, acordDatePersonale, acordRegulament, aiVreaSaSpui } = req.body;
            if (!numeEchipa && !persoanaContact && !email && !numeJucatori && !nivelStudii && !telefon && !undeAflat && !acordDatePersonale && !acordRegulament && !aiVreaSaSpui) {
                res
                  .status(400)
                  .send({ message: "Trebuie sa specifici minim o modificare!"});
            } else {
                CSGODB.findByPk(id)
                 .then(async (echipa) => {
                    if (echipa) {
                        Object.assign(echipa, req.body);
                        await echipa.save();
                        res
                          .status(202)
                          .send({ message: "Echipa a fost actualizata cu succes!"});
                    } else {
                        res.status(404).json({ message: "Echipa nu exista!"});
                    }
                 })
                 .catch((err) => {
                    console.error(err);
                    res.status(500).send({ message: "Eroare de server!"});
                 });
            }
       }, 
       
       deleteEchipaById: (req, res) => {
            const { id } = req.params;
            if (!id) {
                res.status(400).send({ message: "Trebuie sa specifici id-ul!"});
            }

            CSGODB.findByPk(id)
              .then(async (echipa) => {
                if (echipa) {
                    await echipa.destroy();
                    res
                      .status(202)
                      .send({ message: "Echipa a fost stearsa cu succes!"});
                } else {
                    res.status(404).json({ message: "Echipa nu exista!"});
                }
              })
              .catch((err) => {
                console.error(err);
                res.status(500).send({ message: "Eroare de server!"});
              });
       },

       recoverEchipa: (req, res) => {
            const { id } = req.params;
            if (!id) {
                res.status(400).send({ message: "Trebuie sa specifici id-ul!"});
            }

            CSGODB.findByPk(id)
              .then(async (echipa) => {
                if (echipa) {
                    await echipa.restore();
                    res
                      .status(200)
                      .send({ message: "Echipa a fost recuperata cu succes!"});
                } else {
                    res.status(404).json({ message: "Echipa nu exista!"});
                }
            })
              .catch((err) => {
                console.error(err);
                res.status(500).send({ message: "Eroare de server!"});
            });
       },
}

module.exports = controller;