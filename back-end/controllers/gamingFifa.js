const FifaDB = require('../models').Fifa;
const controller = {
    getAllParticipanti: async(req, res)=>{
        let participanti = await FifaDB.findAll()
        .then((fifa)=>{
            res.status(200).send(fifa);
        })
        .catch((error)=>{
            console.log(error);
            res.status(500).send({message:"Eroare la server!"})
        })

    },
    addParticipant: (req, res) => {
        const participantFifa = {
          nume: req.body.nume,
          prenume: req.body.prenume,
          email: req.body.email,
          telefon: req.body.telefon,
          nivelStudii: req.body.nivelStudii,
          undeAflat: req.body.undeAflat,
          aiVreaSaSpui: req.body.aiVreaSaSpui,
        };
    
        let error = [];
        let participantFifaArr = Object.keys(participantFifa);
        let participantFifaVal = Object.values(participantFifa);
        let participantFifaLength = participantFifaArr.length; 
        for (let i = 0; i < participantFifaLength; i++) {
          if (!participantFifaVal[i]) {
            error.push(`Undefined ${participantFifaArr[i]}`);
            console.log(`Undefined ${participantFifaArr[i]}`);
          } else {
            if (participantFifaArr[i] === "nume") {
              console.log(participantFifa.nume);
              if (
                participantFifa.nume.length < 2 &&
                participantFifa.nume.length > 50
              ) {
                error.push(
                  "Numele trebuie sa contina intre 2 si 50 caractere"
                );
                console.log(
                  "Numele trebuie sa contina intre 2 si 50 caractere"
                );
              } else if (
                !participantFifa.nume.match(`^[A-Za-z0-9ăîșțâĂÎȘȚÂ \s]+$`)
              ) {
                error.push("Numele trebuie sa contina litere sau cifre");
                console.log("Numele trebuie sa contina litere sau cifre");
              }
            }
    
            if (participantFifaArr[i] === "prenume") {
              console.log(participantFifa.prenume);
              if (
                participantFifa.prenume.length < 2 &&
                participantFifa.prenume.length > 50
              ) {
                error.push(
                  "Prenumele trebuie sa contina intre 2 si 50 caractere"
                );
                console.log(
                  "Prenumele trebuie sa contina intre 2 si 50 caractere"
                );
              } else if (!participantFifa.prenume.match(`^[A-Za-zăîșțâĂÎȘȚÂ \s]+$`)) {
                error.push("Prenumele trebuie sa contina doar litere");
                console.log("Prenumele trebuie sa contina doar litere");
              }
            }
    
            if (participantFifaArr[i] === "email") {
              console.log(participantFifa.email);
              if (
                !participantFifa.email.includes("@gmail.com") &&
                !participantFifa.email.includes("@yahoo.com") &&
                !participantFifa.email.includes("@hotmail.com")
              ) {
                error.push("Email invalid");
                console.log("Email invalid");
              }
            }
    
            if (participantFifaArr[i] === "telefon") {
              if (
                participantFifa.telefon.length !== 10) {
                error.push("Numarul de telefon trebuie sa contina 10 cifre");
                console.log("Numarul de telefon trebuie sa contina 10 cifre");
              } else if (
                !participantFifa.telefon.substring(0, 2).includes(`07`) &&
                !participantFifa.telefon.substring(2).match(`^[0-9]+$`)
              ) {
                error.push("Numar de telefon invalid");
                console.log("Numar de telefon invalid");
              } 
            } else if (participantFifaArr[i] === "undeAflat") {
              if (
                participantFifa.undeAflat.length < 2 &&
                participantFifa.undeAflat.length > 50
              ) {
                error.push("Ati depasit limita de cuvinte!");
                console.log("Ati depasit limita de cuvinte!");
              }
            }
            if(participantFifa.acordDatePersonale===false || participantFifa.acordTermeni===false )
            {
              error.push("Va rugam sa bifati termenii si conditiile!");
              console.log("Va rugam sa bifati termenii si conditiile!");
            }
          }
        }
    
        if (error.length === 0) {
          FifaDB.create(participantFifa)
            .then((newParticipant) => {
              res.status(201).send(newParticipant);
            })
        } else {
          res.status(400).send({ message: "Ceva nu a mers bine!" });
        }
      },
    
    updateParticipant: (req, res) => {
        const {id} = req.params;
        if(!id){
            res.status(400).send({message: "Trebuie sa specifici id-ul!"});
        }

        const {nume, prenume, email, telefon, nivelStudii, undeAflat, aiVreaSaSpui } = req.body; // AcordDatePersonale, AcordTermeni
        if(!nume && !prenume && !email && !telefon && !nivelStudii && !undeAflat && !aiVreaSaSpui){
            res.status(400).send({ message: "Trebuie sa specifici minim o modificare!"});
        }
        else {
           FifaDB.findByPk(id).then(async (fifa) => {
            if(fifa){
                Object.assign(fifa, req.body);
                await fifa.save();
                res.status(202).send({ message: "Participantul a fost actualizat cu succes!"});
            }else{
                res.status(404).send({message: "Participantul nu exista!"});
            }
           }).catch((err) =>{
            console.error(err);
            res.status(500).send({message: "Eroare de server!"});
           });
        }
    },

    deleteParticipantById: (req,res) =>{
        const{id} = req.params;
        if(!id){
            res.status(400).send({message: "Trebuie sa specifici id-ul!"});
        }
        
        FifaDB.findByPk(id).then(async (fifa) => {
            if(fifa){
                await fifa.destroy();
                res.status(202).send({message: "Participantul a fost sters cu succes!"});              
            }else{
                res.status(404).send({message: "Participantul nu exista!"})
            }
        }).catch((err) => {
            console.error(err);
            res.status(500).send({ message: "Ëroare de server!"})
        });
    },

    recoverParticipant: (req, res) => {
        const { id } = req.params;
        if (!id) {
            res.status(400).send({ message: "Trebuie sa specifici un id!"});
        }

        FifaDB.findByPk(id)
            .then(async (participant) => {
                if (participant) {
                    await participant.restore();
                    res
                      .status(200)
                      .send({ message: "Participantul a fost recuperat cu succes!"});
                } else {
                    res.status(404).json({ message: "Participantul nu exista!"});
                }
            })
            .catch((err) => {
                console.error(err);
                res.status(500).send({ message: "Eroare de server!"});
            });
    }
}

module.exports = controller;