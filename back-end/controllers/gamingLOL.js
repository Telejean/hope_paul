const LOLDB = require('../models').LOL;

const controller = {

       getAllEchipe: (req, res) => {
        LOLDB.findAll()
        .then((echipe) => {
            res.status(200).send(echipe);
        })
        .catch((err) => {
            console.log(err);
            res.status(200).send({message: 'Server error!'});
        })
       },
       
       addEchipa: (req, res) => {
        const echipaLOL = {
            numeEchipa: req.body.numeEchipa,
            persoanaContact: req.body.persoanaContact,
            email: req.body.email,
            numeJucatori: req.body.numeJucatori,
            nivelStudii: req.body.nivelStudii,
            telefon: req.body.telefon,
            undeAflat: req.body.undeAflat,
            aiVreaSaSpui: req.body.aiVreaSaSpui,
        }
        let error = [];
    let echipaLOLVal = Object.values(echipaLOL);
    let echipaLOLArr = Object.keys(echipaLOL);
    for (let camp = 0; camp < echipaLOLArr.length; camp++) {
      if (echipaLOLArr[camp] === undefined || echipaLOLArr[camp] === "") {
        error.push(`undefined ${echipaLOLArr[camp]}`);
        console.log(`Nu a fost completat campul ${echipaLOLArr[camp]}`);
      } else {
        if (echipaLOLArr[camp] == "numeEchipa") {
          if (echipaLOL.numeEchipa.length > 100) {
            error.push(`Numele echipei poate contine maxim 100 de caractere`);
            console.log(`Numele echipei poate contine maxim 100 de caractere`);
          } else if (
            !echipaLOL.numeEchipa.match(`^[A-Za-z0-9ăîșțâĂÎȘȚÂ \s]+$`)
          ) {
            error.push(`Numele echipei poate contine doar litere si cifre`);
            console.log(`Numele echipei poate contine doar litere si cifre`);
          }
        } else if (echipaLOLArr[camp] == "persoanaContact") {
          if (echipaLOL.persoanaContact.length > 50) {
            error.push(`Numele persoanei de contact este prea lung`);
            console.log(`Numele persoanei de contact este prea lung`);
          } else if (
            !echipaLOL.persoanaContact.match(`^[A-Za-zăîșțâĂÎȘȚÂ \s]+$`)
          ) {
            error.push(`Numele persoanei de contact poate contine doar litere`);
            console.log(`Numele persoanei de contact poate contine doar litere`);
          }
        } else if (echipaLOLArr[camp] == "email") {
          if (
            !echipaLOL.email.includes("@gmail.com") &&
            !echipaLOL.email.includes("@yahoo.com") &&
            !echipaLOL.email.includes("@hotmail.com")
          ) {
            error.push(`Emailul introdus este invalid`);
            console.log(`Emailul introdus este invalid`);
          }
        } else if (echipaLOLArr[camp] == "telefon") {
          if (echipaLOL.telefon.length !== 10) {
            console.log(`Numarul de telefon este invalid1`);
            error.push(`Numarul de telefon este invalid1`);
          } else {
            if (
              !echipaLOL.telefon.substring(0, 2).includes(`07`) && !echipaLOL.telefon.substring(2).match(`^[0-9]+$`)
            ) {
              error.push(`Numarul de telefon este invalid2`);
              console.log(`Numarul de telefon este invalid2`);
            }
          }
        }
      }

      if (echipaLOLArr[camp] == "numeJucatori")
      {if (echipaLOL.numeJucatori.length > 150) {
        console.log(
          'Campul "Numele jucatorilor" poate contine maxim 150 de caractere!'
        );
        error.push(
          'Campul "Numele jucatorilor" poate contine maxim 150 de caractere!'
        );
      }
      if (
        !echipaLOL.numeJucatori.match(`^[A-Za-zăîșțâĂÎȘȚÂ, \s ]+$`)
      ) {
        error.push("Numele jucatorilor trebuie sa contina doar litere");
        console.log("Numele jucatorilor trebuie sa contina doar litere");
      }
    }
        
        
      if (echipaLOLArr[camp] == "undeAflat")
        if (echipaLOL.undeAflat.length > 500) {
          error.push("Campul poate contine maxim 500 de caractere!");
          console.log("Campul poate contine maxim 500 de caractere!");
        }
    }

    if (error.length === 0) {
      LOLDB.create(echipaLOL)
        .then((newEchipa) => {
          res.status(201).send(newEchipa);
        })
    } else {
     // res.status(500).send({ message: "Server error!" });
     
      console.log(err);
      res.status(400).send({message: "Unul sau mai multe campuri nu au fost completate!"})
    }
  },
       
       updateEchipaById: (req, res) =>{
            const { id } = req.params;
            if (!id) {
                res.status(400).send({ message: "Trebuie sa specifici id-ul!"});
            }

            const { numeEchipa, persoanaContact, email, numeJucatori, nivelStudii, telefon, undeAflat, acordDatePersonale, acordRegulament, aiVreaSaSpui } = req.body;
            if (!numeEchipa && !persoanaContact && !email && !numeJucatori && !nivelStudii && !telefon && !undeAflat && !acordDatePersonale && !acordRegulament && !aiVreaSaSpui) {
                res
                  .status(400)
                  .send({ message: "Trebuie sa specifici minim o modificare!"});
            } else {
                LOLDB.findByPk(id)
                 .then(async (echipa) => {
                    if (echipa) {
                        Object.assign(echipa, req.body);
                        await echipa.save();
                        res
                          .status(202)
                          .send({ message: "Echipa a fost actualizata cu succes!"});
                    } else {
                        res.status(404).json({ message: "Echipa nu exista!"});
                    }
                 })
                 .catch((err) => {
                    console.error(err);
                    res.status(500).send({ message: "Eroare de server!"});
                 });
            }
       }, 
       
       deleteEchipaById: (req, res) => {
            const { id } = req.params;
            if (!id) {
                res.status(400).send({ message: "Trebuie sa specifici id-ul!"});
            }

            LOLDB.findByPk(id)
              .then(async (echipa) => {
                if (echipa) {
                    await echipa.destroy();
                    res
                      .status(202)
                      .send({ message: "Echipa a fost stearsa cu succes!"});
                } else {
                    res.status(404).json({ message: "Echipa nu exista!"});
                }
              })
              .catch((err) => {
                console.error(err);
                res.status(500).send({ message: "Eroare de server!"});
              });
       },

       recoverEchipa: (req, res) => {
            const { id } = req.params;
            if (!id) {
                res.status(400).send({ message: "Trebuie sa specifici id-ul!"});
            }

            LOLDB.findByPk(id)
            .then(async (echipa) => {
                if (echipa) {
                    await echipa.restore();
                    res
                      .status(200)
                      .send({ message: "Echipa a fost recuperata cu succes!"});
                } else {
                    res.status(404).json({ message: "Echipa nu exista!"});
                }
            })
            .catch((err) => {
                console.error(err);
                res.status(500).send({ message: "Eroare de server!"});
            });
       },
}

module.exports = controller;