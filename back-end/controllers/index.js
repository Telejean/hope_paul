const admin = require('./admin');
const baschet = require('./baschet');
const fotbal = require('./fotbal');
const fifa = require('./gamingFifa');
const LOL = require('./gamingLOL');
const CSGO = require('./gamingCSGO');


const controllers = {
    admin, 
    baschet, 
    fotbal, 
    fifa,
    LOL,
    CSGO, 
};

module.exports = controllers;