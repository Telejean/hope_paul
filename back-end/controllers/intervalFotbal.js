const IntervalFotbalDb = require("../models").IntervalFotbal;

const controller = {
    getAllIntervals: async (req, res) => {
        try {
            let intervale = await IntervalFotbalDb.findAll();
            res.status(201).send(intervale);
        } catch (err) {
            res.status(500).send(`Server error! Error: ${err.message}`);
        }
    },
};

module.exports = controller;
