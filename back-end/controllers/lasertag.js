const LasertagDb = require("../models").Lasertag;



const controller = {
    getAllParticipantsLasertag: async (req, res) => {
        try {
            let participantsLasertag = await LasertagDb.findAll();
            if (!participantsLasertag.length) throw new Error("gol");
            res.status(200).send(participantsLasertag);
        } catch (err) {
            if (err.message === "gol")
                res.status(404).send("Nu exista participanti la lasertag!");

            else
                res.status(500).send({ message: "Server error!" });
        }
    },

    //ruta de GET pentru aflarea nume+telefoanelor participantilor  (nu stiu daca ajuta dar sa fie acolo)    ---------------------------
    getAllTelefonLasertag: async (req, res) => {
        try {
            let participantsLasertag = await LasertagDb.findAll();


            let participants = [];
            for (let participant of participantsLasertag) {
                let p = {
                    nume: participant.nume,
                    telefon: participant.telefon
                }
                participants.push(p);
            }
            res.status(200).send(participants);
        } catch (err) {

        }


    },

    createParticipantLasertag: async (req, res) => {
        try {
            const participantLasertag = {
                nume: req.body.nume,
                email: req.body.email,
                telefon: req.body.telefon,
                studii: req.body.studii,
                undeAflat: req.body.undeAflat
            }

            for (let camp in participantLasertag) {
                if (participantLasertag[camp] === undefined || participantLasertag[camp] === "") {
                    throw new Error("undefined");
                }
            }

            let newParticipant = await LasertagDb.create(participantLasertag);
            res.status(201).send(newParticipant);

        } catch (err) {

            if (err.message === "undefined")
                res.status(418).send("Unul sau mai multe campuri nu au fost completate!");

            else
                res.status(500).send({ message: "Server error!" });
        }

    },

    updateParticipaLasertag: async (req, res) => {
        try{
            const id = req.params.id;
            let entry = await LasertagDb.findOne({where: {
                lasertagId: id
            }});
            if(entry)
            {
                await entry.update({participa : !entry.participa})
                entry = await entry.save();
                res.status(200).send(entry);
            }
            else 
            {
                res.status(404).send("Nu exista inregistrare cu id-ul cerut");
            }
        }
        catch(err) 
        {   
            res.status(500).send("Server error!");
            console.log(err.message);
        }
    },

    deleteLasertagByEmail: async (req,res) => {
        try {
            const email = req.body.email;
            let entry = await EscapeDb.findOne({where: {
                email: email
            }});
            console.log(JSON.stringify(entry));
            if(entry)
            {
                await entry.destroy({force: true});
                res.status(200).send("Echipa stearsa!");
            }
            else 
            {
                res.status(404).send("Nu exista inregistrare cu email-ul cerut");
            }
        }
        catch (err) {
            res.status(500).send("Server error!");
            console.log(err.message);
        }
    }
}

module.exports = controller;