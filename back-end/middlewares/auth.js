const cookie = require('cookie');

const auth = {
    verAuth:(req,res,next)=>{
        if(req.isAuthenticated())
        {
            return next();
        }else
        {
            res.status(401).send({message:"Utilizator neautentificat"})
        }
    },

    verAuthFront:(req,res,next)=>{
        if(req.isAuthenticated())
        {
            res.status(200).send({message:"Succes"})
        }else
        {
            res.status(401).send({message:"Utilizator neautentificat"})
        }
    }
}

module.exports = auth;