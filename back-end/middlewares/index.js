const multer = require("./multer");
const auth = require("./auth");

module.exports = {
    multer,
    auth,
};