const path = require('path');
const multer = require("multer");

let storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './files/');
    },

    filename: (req, file, callback) => {
        const fileExtension = path.extname(file.originalname);
        callback(null, req.body.nume + req.body.prenume + fileExtension);
    }
});

const fileFilter = (req, file, callback) => {
    let ext = path.extname(file.originalname);
    if (ext !== '.pdf' && ext !== '.PDF') {
        return callback(new Error('Only documents are allowed'), false);
    }

    let participant = req.body;
    let ok = true;
    for (let camp in participant) {
        if (participant[camp] === undefined || participant[camp] === "") {
            ok = false;
            break;
        }
    }

    if (!ok)
        return callback(new Error('Date incomplete'), false);

    callback(null, true);
};


let upload = multer({
    storage: storage,
    limits: {
        files: 1,
        fileSize: 1024 * 1024 * 5,
    },
    fileFilter: fileFilter,
});

module.exports = upload;