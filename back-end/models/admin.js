module.exports = (sequelize, DataTypes) => {
    const admin = sequelize.define("admin", {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey:true
        },
        email: {
            type:DataTypes.STRING,
            required: true
        },
    });

    return admin;
};
