module.exports = (sequelize, DataTypes) => {
  const fotbal = sequelize.define(
    "fotbal",
    {
      fotbalId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },

      numeEchipa: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },

      studiiCapitan: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },

      numeCapitan: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },

      emailCapitan: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },

      telefonCapitan: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },

      numeCoechipieri: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },

      //   interval: {
      //     type: DataTypes.STRING,
      //     allowNull: false,
      //     validate: {
      //       notEmpty: true,
      //     },
      //   },

      undeAflat: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },

      // participa: {
      //   type: DataTypes.BOOLEAN,
      //   allowNull: true,
      //   defaultValue: false,
      //   validate: {
      //     notEmpty: false,
      //   },
      // },

      aiVreaSaSpui: {
        type: DataTypes.STRING,
        allowNull: true,
      },


      deletedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: true,
    }
  );

  return fotbal;
};
