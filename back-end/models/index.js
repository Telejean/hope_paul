const Sequelize = require("sequelize");
const db = require("../config/db");

const AdminModel = require("./admin");
const BaschetModel = require("./baschet");
const LasertagModel = require("./lasertag");
const FotbalModel = require("./fotbal");
const FifaModel = require("./gamingFifa");
const LOLModel = require("./gamingLOL");
const CSGOModel = require("./gamingCSGO");
//const EscapeModel = require("./escape");
// const IntervalBaschetModel = require("./intervalBaschet");
// const IntervalFotbalModel = require("./intervalFotbal");

const Admin = AdminModel(db, Sequelize);
const Baschet = BaschetModel(db, Sequelize);
const Fotbal = FotbalModel(db, Sequelize);
const Fifa = FifaModel(db, Sequelize);
const LOL = LOLModel(db, Sequelize);
const CSGO = CSGOModel(db, Sequelize);
const Lasertag = LasertagModel(db, Sequelize);
//const Escape = EscapeModel(db, Sequelize);
// const IntervalBaschet = IntervalBaschetModel(db, Sequelize);
// const IntervalFotbal = IntervalFotbalModel(db, Sequelize);

module.exports = {
  Baschet,
  Fotbal,
  Fifa,
  LOL,
  CSGO,
  Lasertag,
  // IntervalBaschet,
  // IntervalFotbal,
  Admin,
  connection: db,
};
