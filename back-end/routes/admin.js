const express = require("express");
const router = express.Router();

const adminController = require("../controllers").admin;

router.get("/:email", adminController.getAdminByEmail );
router.post("/", adminController.createAdmin);

module.exports = router;
