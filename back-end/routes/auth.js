const express = require('express')
const passport = require("passport");
const router = express.Router();
const isAuth = require("../middlewares/auth");

router.get(
    "/google",
    passport.authenticate("google", {scope: ["profile", "email"]})

);

router.get("/google/redirect",passport.authenticate("google", { scope: ["profile", "email"] }),
(req, res) =>{
  
   res.cookie('isAuthenticated','123')

  res.redirect("/admin")

})

router.get("/", isAuth.verAuthFront)


module.exports = router;