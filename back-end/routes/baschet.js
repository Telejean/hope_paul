const express = require("express");
const { verAuth } = require("../middlewares/auth");
const router = express.Router();

const baschetController = require("../controllers").baschet;

const multerUpload = require("../middlewares").multer;

router.get("/getAll", verAuth, baschetController.getAllEchipeBaschet);

router.post("/create", [multerUpload.none()], baschetController.createEchipaBaschet);

router.put("/:id", baschetController.updateParticipaBaschet);

router.delete("/delete/:id", baschetController.deleteBaschetById);
router.put("/undo/:id", baschetController.recoverEchipaBaschet);

module.exports = router;