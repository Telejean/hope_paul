const express = require("express");
const router = express.Router();

const fotbalController = require("../controllers").fotbal;

const multerUpload = require("../middlewares").multer;

router.get("/getAll", fotbalController.getAllEchipeFotbal);

router.post("/create",[multerUpload.none()],fotbalController.createEchipaFotbal);

router.put("/:id", fotbalController.updateParticipaFotbal);

router.delete("/delete/:id", fotbalController.deleteFotbalById);
router.put("/undo/:id", fotbalController.recoverEchipaFotbal);


module.exports = router;
