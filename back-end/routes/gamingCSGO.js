const express = require("express");
const router = express.Router();
const CSGOController = require("../controllers").CSGO;
const multerUpload = require("../middlewares").multer;

router.get('/', CSGOController.getAllEchipe);
router.post("/", CSGOController.addEchipa);
router.put("/:id", CSGOController.updateEchipaById);
router.delete("/:id", CSGOController.deleteEchipaById);
router.put("/undo/:id", CSGOController.recoverEchipa);
router.post("/create", [multerUpload.none()], CSGOController.addEchipa);

module.exports = router;
