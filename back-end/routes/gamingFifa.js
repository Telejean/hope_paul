const express = require("express");
const router = express.Router();
const fifaController = require("../controllers").fifa;
const multerUpload = require('../middlewares').multer;

router.get('/', fifaController.getAllParticipanti);
router.post('/', fifaController.addParticipant);
router.put('/:id', fifaController.updateParticipant);
router.delete('/:id', fifaController.deleteParticipantById);
router.put('/undo/:id', fifaController.recoverParticipant);
router.post("/create", [multerUpload.none()], fifaController.addParticipant);

module.exports = router;