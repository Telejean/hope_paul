const express = require("express");
const router = express.Router();
const LOLController = require("../controllers").LOL;
const multerUpload = require("../middlewares").multer;

router.get('/', LOLController.getAllEchipe);
router.post("/", LOLController.addEchipa);
router.put("/:id", LOLController.updateEchipaById);
router.delete("/:id", LOLController.deleteEchipaById);
router.put("/undo/:id", LOLController.recoverEchipa);
router.post("/create", [multerUpload.none()], LOLController.addEchipa);

module.exports = router;

