const express = require("express");
const router = express.Router();
const adminRouter = require("./admin");
const baschetRouter = require("./baschet");
const fotbalRouter = require("./fotbal");

const fifaRouter = require("./gamingFifa");
const LOLRouter = require("./gamingLOL");
const CSGORouter = require("./gamingCSGO");

// if(process.env.NODE_ENV !== 'production') {
//     router.use('/', otherRouter);
// }
const escapeRouter = require("./escape");
const authRouter = require("./auth")
// const intervalBaschetRouter = require("./intervalBaschet");
// const intervalFotbalRouter = require("./intervalFotbal");

router.use("/admin", adminRouter);
router.use("/baschet", baschetRouter);
router.use("/fotbal", fotbalRouter);

router.use("/fifa", fifaRouter);
router.use("/LOL", LOLRouter);
router.use("/CSGO", CSGORouter);

//router.use("/lasertag", lasertagRouter);
//router.use("/escape", escapeRouter);
router.use("/auth" , authRouter);

router.get("/reset", (req, res) => {
  connection
    .sync({ force: true })
    .then(() => {
      res.status(200).send({ message: "Database reset completed!" });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ message: "Database reset failed!" });
    });
});

// router.use("/intervalBaschet", intervalBaschetRouter);
// router.use("/intervalFotbal", intervalFotbalRouter);

module.exports = router;
