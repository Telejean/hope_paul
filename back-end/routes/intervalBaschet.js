const express = require("express");
const router = express.Router();

const intervalBaschetController = require("../controllers").intervalBaschet;

router.get("/getAll", intervalBaschetController.getAllIntervals);

module.exports = router;
