const express = require("express");
const router = express.Router();

const intervalFotbalController = require("../controllers").intervalFotbal;

router.get("/getAll", intervalFotbalController.getAllIntervals);

module.exports = router;
