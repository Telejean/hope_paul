const express = require("express");
const router = express.Router();

const lasertagController = require("../controllers").lasertag;

const multerUpload = require("../middlewares").multer;


router.get("/getAll", lasertagController.getAllParticipantsLasertag);

router.get("/getTelefon", lasertagController.getAllTelefonLasertag);

router.post("/create", [multerUpload.none()], lasertagController.createParticipantLasertag);

router.put("/:id", lasertagController.updateParticipaLasertag);

router.delete("/delete", lasertagController.deleteLasertagByEmail);

module.exports = router;