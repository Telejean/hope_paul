require("dotenv").config();

const express = require("express");
const cors = require("cors");
const connection = require("./models").connection;
const router = require("./routes");
const { error } = require("toastr");

const passport = require("passport");
const cookieSession = require("cookie-session");
const { verAuth } = require("./middlewares/auth");
const expressSession= require("express-session")
require("./controllers/auth");

const app = express();
//let port = 3693;
let port = 7070;

app.use(express.json());

app.use(cors());

app.use(expressSession({
    secret:"macaca",
    resave: true,
    saveUninitialized: true
}))

app.use(passport.initialize());
app.use(passport.session());

// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", `http://localhost:8080`);
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   next();
// });

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", `https://hope.sisc.ro`);
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.get("/reset", function (req, res) {
  connection.sync({force:true})
  .then(() => {
    res.status(200).send({message: "Baza de date a fost resetata cu succes"})
  })
  .catch((error) => {
    res.status(500).send(error)
  }) 
})

app.use("/api", router);

app.get("/admin",verAuth, express.static("../front-end/admin"));

app.use("/login",express.static("../front-end/logare") )

app.use("/succesFotbal", express.static("../front-end/formulare/fotbal/validare"));

app.use(express.static("../front-end/"));


app.listen(port, () => {
  console.log(`Server is running on ${port}`);
  console.log(`http://localhost:${port}`);
});
