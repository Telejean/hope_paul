function load() {
  
  request.onload = function () {
    if () {
      let divToInsert = document.getElementById("toLoad");
      divToInsert.innerHTML = `
      <div class="row">
        <a href="./baschet/baschet.html">
          <img src="../imagini/admin/basketball-ball.png" alt="" />
          Basket
        </a>
        <button onclick="downloadCSVBaschet()">Exporta CSV Baschet</button>
      </div>   
      <div class="row">
        <a href="./escape/escape.html"> <img src="../imagini/admin/room.png" alt="" />Escape</a>
        <button onclick="downloadCSVEscape()">Exporta CSV Escape</button>
      </div>
      <div class="row">
        <a href="./football/football.html ">
          <img src="../imagini/admin/football.png" alt="" />
          Football
        </a>
        <button onclick="downloadCSVFotbal()">Exporta CSV Fotbal</button>  
      </div>
      <div class="row">
        <a href="./laser/laser.html">
          <img src="../imagini/admin/gun.png" alt="" />
          Laser
        </a> 
        <button onclick="downloadCSVLaser()">Exporta CSV Laser</button>
      </div>
      `;
      return;
    } else {
      //window.location.replace("https://hope.sisc.ro/");
      window.location.replace("http://localhost:8080/");
    }
  };

  request.send();
}

function transformDataToCSV(data) {
  return data
    .map((row) => Object.values(row).map(String).join(","))
    .join("\r\n");
}

function downloadCSVBaschet() {
  const urlBaschet = "https://hope.sisc.ro/api/baschet/getAll";
  let getBaschet = new XMLHttpRequest();
  getBaschet.open("GET", urlBaschet);
  getBaschet.responseType = "json";

  getBaschet.onload = function () {
    let data = getBaschet.response;
    const headers =
      "Nr. Curent, Nume Echipa, Nume Capitan, Studii Capitan, Email Capitan, Telefon Capitan, Nume Coechipier 1, Nume Coechipier 2, Nume Rezerva 1, De unde a aflat, Participa\n";
    let content = transformDataToCSV(data);
    content = headers + content;
    const blob = new Blob([content], { type: "text/csv;charset=utf-8;" });
    const url = URL.createObjectURL(blob);

    var pom = document.createElement("a");
    pom.href = url;
    pom.setAttribute("download", "export_baschet.csv");
    pom.click();
  };
  getBaschet.send();
}

function downloadCSVFotbal() {
  const urlFotbal = "https://hope.sisc.ro/api/fotbal/getAll";
  let getFotbal = new XMLHttpRequest();
  getFotbal.open("GET", urlFotbal);
  getFotbal.responseType = "json";

  getFotbal.onload = function () {
    let data = getFotbal.response;
    const headers =
      "Nr. Curent, Nume Echipa, Nume Capitan, Studii Capitan, Email Capitan, Telefon Capitan, Nume Coechipier 1, Nume Coechipier 2, Nume Coechipier 3, Nume Coechipier 4, Nume Coechipier 5, Nume Rezerva 1, Nume Rezerva 2, De unde a aflat, Participa\n";
    let content = transformDataToCSV(data);
    content = headers + content;
    const blob = new Blob([content], { type: "text/csv;charset=utf-8;" });
    const url = URL.createObjectURL(blob);

    var pom = document.createElement("a");
    pom.href = url;
    pom.setAttribute("download", "export_fotbal.csv");
    pom.click();
  };
  getFotbal.send();
}

function downloadCSVEscape() {
  const urlEscape = "https://hope.sisc.ro/api/escape/getAll";
  let getEscape = new XMLHttpRequest();
  getEscape.open("GET", urlEscape);
  getEscape.responseType = "json";

  getEscape.onload = function () {
    let data = getEscape.response;
    const headers =
      "Nr. Curent, Nume, Email, Telefon, Studii, De unde a aflat, Participa\n";
    let content = transformDataToCSV(data);
    content = headers + content;
    const blob = new Blob([content], { type: "text/csv;charset=utf-8;" });
    const url = URL.createObjectURL(blob);

    var pom = document.createElement("a");
    pom.href = url;
    pom.setAttribute("download", "export_escape.csv");
    pom.click();
  };
  getEscape.send();
}

function downloadCSVLaser() {
  const urlLaser = "https://hope.sisc.ro/api/lasertag/getAll";
  let getLaser = new XMLHttpRequest();
  getLaser.open("GET", urlLaser);
  getLaser.responseType = "json";

  getLaser.onload = function () {
    let data = getLaser.response;
    const headers = "Nume, Email, Telefon, Studii, De unde a aflat\n";
    let content = transformDataToCSV(data);
    content = headers + content;
    const blob = new Blob([content], { type: "text/csv;charset=utf-8;" });
    const url = URL.createObjectURL(blob);

    var pom = document.createElement("a");
    pom.href = url;
    pom.setAttribute("download", "export_laser.csv");
    pom.click();
  };
  getLaser.send();
}
