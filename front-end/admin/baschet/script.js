function load() {
  
  const url = `http://localhost:7070/api/auth/`;
  //const url = `https://hope.sisc.ro/api/admin?password=${sign}`;

  let request = new XMLHttpRequest();
  request.open("GET", url);
  request.responseType = "json";
  request.onload = function () {
    console.log(request.response)
    if (request.response["message"]==="Succes") {

      const urlBaschet = "http://localhost:7070/api/baschet/getAll";
      //const urlBaschet = "https://hope.sisc.ro/api/baschet/getAll";

      let getBaschet = new XMLHttpRequest();
      getBaschet.open("GET", urlBaschet);
      getBaschet.responseType = "json";

      getBaschet.onload = function () {
        let insertTable = document.getElementById("insertTheTableHere");
        let data = getBaschet.response;

        for (let i = 0; i < data.length; i++) {
          if (data[i].deletedAt !== "NULL") {
            ok = 1;
          }
          else {
            ok = 0;
          }

          const nrCurrent = i + 1;
          insertTable.innerHTML += `<tr key=${nrCurrent}}> 
          <td>${data[i].baschetId}</td>
          <td>${data[i].numeEchipa}</td>
          <td>${data[i].numeCapitan}</td>
          <td>${data[i].studiiCapitan}</td>
          <td>${data[i].emailCapitan}</td>
          <td>${data[i].telefonCapitan}</td>
          <td>${data[i].numeCoechipier1}</td>
          <td>${data[i].numeCoechipier2}</td>
          <td>${data[i].numeRezerva}</td>
          <td>${data[i].undeAflat}</td>
          <td><input type="checkbox" onchange="updateEchipa(${data[i].baschetId
            }" ${data[i].participaBaschet ? "checked" : ""}></td>
          <td><button id="delete${data[i].baschetId}" class=${ok == 0 ? "btn-1a-disable" : "btn-1a"
            } onclick="deleteEchipa(${data[i].baschetId
            })">Delete</button></td>
          <td><button id="undo${data[i].baschetId}" class=${ok == 1 ? "btn-1b-disable" : "btn-1b"
            } onclick="undoEchipa(${data[i].baschetId})">Undo</button></td>

          </tr>`;

          //TODO: de adaugat la input functie de updateParticipa
          //TODO: de adagat la buton functie de delete
        }
      };

      getBaschet.send();
    } else {
      window.location.replace("http://localhost:7070/login");
    }
  };

  request.send();
}

function deleteEchipa(idEchipa) {
  console.log(idEchipa);
  const urlDeleteBaschet = `https://hope.sisc.ro/api/baschet/delete/${idEchipa}`; //delete
  let deleteBaschet = new XMLHttpRequest();
  deleteBaschet.open("DELETE", urlDeleteBaschet);
  deleteBaschet.responseType = "json";

  let deleteButon = document.getElementById(`delete${idEchipa}`);
  deleteButon.setAttribute("disabled", "");
  deleteButon.setAttribute("class", "btn-1a-disable")

  let undoButton = document.getElementById(`undo${idEchipa}`);
  undoButton.removeAttribute("disabled");
  undoButton.setAttribute("class", "btn-1b")
  //de facut ruta de delete in backend si continuat functia;
}

function updateEchipa(idEchipa) {
  console.log(idEchipa);
  const urlParticipaBaschet = `https://hope.sisc.ro/api/baschet/${idEchipa}`; //put
  let participaBaschet = new XMLHttpRequest();
  participaBaschet.open("PUT", urlParticipaBaschet);
  participaBaschet.responseType = "json";
  // sa se faca ruta de participa si continuat functia;
}

function undoEchipa(idEchipa) {
  console.log(idEchipa);
  const urlUndoBaschet = `https://hope.sisc.ro/api/baschet/undo/${idEchipa}`;
  let undoBaschet = new XMLHttpRequest();
  undoBaschet.open("PUT", urlUndoBaschet);
  undoBaschet.responseType = "json";

  let undoButton = document.getElementById(`undo${idEchipa}`)
  undoButton.setAttribute("disabled", "");
  undoButton.setAttribute("class", "btn-1b-disable");

  let deleteButton = document.getElementById(`delete${idEchipa}`)
  deleteButton.removeAttribute("disabled");
  deleteButton.setAttribute("class", "btn-1a");
}
