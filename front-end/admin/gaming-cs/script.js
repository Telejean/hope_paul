function load() {

  const url = `http://localhost:7070/api/auth/`;

  let request = new XMLHttpRequest();
request.open("GET", url);
request.responseType = "json";
request.onload = function () {
  console.log(request.response)
  if (request.response["message"]==="Succes") {
      const urlFotbal = "http://localhost:7070/api/fotbal/getAll";
      //const urlFotbal = "https://hope.sisc.ro/api/fotbal/getAll";
      let getFotbal = new XMLHttpRequest();
      getFotbal.open("GET", urlFotbal);
      getFotbal.responseType = "json";

      getFotbal.onload = function () {
        let insertTable = document.getElementById("insertTheTableHere");
        let data = getFotbal.response;

        for (let i = 0; i < data.length; i++) {
          console.log(data[i]);
          if (data[i].deletedAt !== "NULL") {
            ok = 1;
          }
          else {
            ok = 0;
          }

          const nrCurrent = i + 1;
          insertTable.innerHTML +=
            `<tr key=${nrCurrent}}> 
          <td>${data[i].fotbalId}</td>
          <td>${data[i].numeEchipa}</td>
          <td>${data[i].numeCapitan}</td>
          <td>${data[i].studiiCapitan}</td>
          <td>${data[i].emailCapitan}</td>
          <td>${data[i].telefonCapitan}</td>
          <td>${data[i].numeCoechipier1}</td>
          <td>${data[i].numeCoechipier2}</td>
          <td>${data[i].numeCoechipier3}</td>
          <td>${data[i].numeCoechipier4}</td>
          <td>${data[i].numeCoechipier5}</td>
          <td>${data[i].numeRezerva1}</td>
          <td>${data[i].numeRezerva2}</td>
          <td>${data[i].undeAflat}</td>
          <td><input type="checkbox" onchange="updateEchipa(${data[i].fotbalId})" ${data[i].participaFotbal ? "checked" : ""}></td>
          <td><button id="delete${data[i].fotbalId}" class=${ok == 0 ? "btn-1a-disable" : "btn-1a"
            } onclick="deleteEchipa(${data[i].fotbalId
            })">Delete</button></td>
          <td><button id="undo${data[i].fotbalId}"class=${ok == 1 ? "btn-1b-disable" : "btn-1b"
            } onclick="undoEchipa(${data[i].fotbalId})">Undo</button></td>          </tr>`
        }
      };

      getFotbal.send();
    } else {
      //window.location.replace("https://hope.sisc.ro/");
      window.location.replace("http://localhost:7070/");
    }
  };

  request.send();
}

function deleteEchipa(idEchipa) {
  console.log(idEchipa);
  const urlDeleteFotbal = `https://hope.sisc.ro/api/fotbal/delete/${idEchipa}` //put
  let deleteFotbal = new XMLHttpRequest();
  deleteFotbal.open("DELETE", urlDeleteFotbal);
  deleteFotbal.responseType = "json";

  let deleteButon = document.getElementById(`delete${idEchipa}`);
  deleteButon.setAttribute("disabled", "");
  deleteButon.setAttribute("class", "btn-1a-disable")

  let undoButton = document.getElementById(`undo${idEchipa}`);
  undoButton.removeAttribute("disabled");
  undoButton.setAttribute("class", "btn-1b")
  //de facut ruta de delete in backend si continuat functia;
}

function updateEchipa(idEchipa) {
  console.log(idEchipa);
  const urlParticipaFotbal = `https://hope.sisc.ro/api/fotbal/${idEchipa}` //put
  let participaFotbal = new XMLHttpRequest();
  participaFotbal.open("PUT", urlParticipaFotbal);
  participaFotbal.responseType = "json";
  // sa se faca ruta de participa si continuat functia;
}

function undoEchipa(idEchipa) {
  console.log(idEchipa);
  const urlUndoFotbal = `https://hope.sisc.ro/api/fotbal/undo/${idEchipa}`;
  let undoFotbal = new XMLHttpRequest();
  undoFotbal.open("PUT", urlUndoFotbal);
  undoFotbal.responseType = "json";

  let undoButton = document.getElementById(`undo${idEchipa}`)
  undoButton.setAttribute("disabled", "");
  undoButton.setAttribute("class", "btn-1b-disable");

  let deleteButton = document.getElementById(`delete${idEchipa}`)
  deleteButton.removeAttribute("disabled");
  deleteButton.setAttribute("class", "btn-1a");
}