let modalaDonatii = document.getElementById("modala");
let donatiiButon = document.getElementById("donatii");
let xButton = document.getElementById("close");

donatiiButon.onclick = function () {
  if (modalaDonatii.style.display === "block") {
    modalaDonatii.style.display = "none";
  } else {
    modalaDonatii.style.display = "block";
  }
};

xButton.onclick = function () {
  modalaDonatii.style.display = "none";
};
