const sidebar = document.getElementById("sidebar");
const menuBotton=document.getElementById("meniu");
sidebar.style.left="0";
let isClosed = false;

const moveSidebar = () => {
    if (window.scrollY > 500 && !isClosed) {
        sidebar.style.left = "-6rem";
        isClosed = true;
    }

    if (window.scrollY < 500 && isClosed) {
        sidebar.style.left = "0";
        isClosed = false;
    }
}
const onHoverSidebar = () => {
    sidebar.style.left = "0";
}

const onExitSideBar = () => {
    if (isClosed)
    sidebar.style.left = "-6rem";
}

const switchMenu = () => {
    if(isClosed)
    {
        sidebar.style.left = "0";
    }
    else
    {
        sidebar.style.left = "-6rem";
    }
    
    isClosed=!isClosed;
}

menuBotton.addEventListener("click", switchMenu);

if (window.innerWidth > 768) {
    window.addEventListener("scroll", moveSidebar, false);
    sidebar.addEventListener("mouseenter", onHoverSidebar, false);
    sidebar.addEventListener("mouseleave", onExitSideBar, false);
}


