window.onload = () => {
  

    const inscriereBaschet = {
      numeEchipa: document.getElementById("numeEchipa").value,
      numeCapitan: document.getElementById("numeCapitan").value,
      prenumeCapitan: document.getElementById("prenumeCapitan").value,
      emailCapitan: document.getElementById("emailCapitan").value,
      telefonCapitan: document.getElementById("telefonCapitan").value,
      studiiCapitan: document.getElementById("nivelStudii").value,
      numeCoechipier1: document.getElementById("numeCoechipier1").value,
      prenumeCoechipier1: document.getElementById("prenumeCoechipier1").value,
      numeCoechipier2: document.getElementById("numeCoechipier2").value,
      prenumeCoechipier2: document.getElementById("prenumeCoechipier2").value,
      numeRezerva: document.getElementById("numeRezerva").value,
      prenumeRezerva: document.getElementById("prenumeRezerva").value,
      undeAflat: document.getElementById("info").value,
      regulament: document.getElementById("regulament").checked,
      gdpr: document.getElementById("gdpr").checked,
    };

    let inscriereBaschetFormData = new FormData();
    inscriereBaschetFormData.append("numeEchipa", inscriereBaschet.numeEchipa);
    inscriereBaschetFormData.append(
      "numeCapitan",
      inscriereBaschet.numeCapitan + " " + inscriereBaschet.prenumeCapitan
    );
    inscriereBaschetFormData.append(
      "emailCapitan",
      inscriereBaschet.emailCapitan
    );
    inscriereBaschetFormData.append(
      "telefonCapitan",
      inscriereBaschet.telefonCapitan
    );
    inscriereBaschetFormData.append(
      "studiiCapitan",
      inscriereBaschet.studiiCapitan
    );
    inscriereBaschetFormData.append(
      "numeCoechipier1",
      inscriereBaschet.numeCoechipier1 +
        " " +
        inscriereBaschet.prenumeCoechipier1
    );
    inscriereBaschetFormData.append(
      "numeCoechipier2",
      inscriereBaschet.numeCoechipier2 +
        " " +
        inscriereBaschet.prenumeCoechipier2
    );
    inscriereBaschetFormData.append(
      "numeRezerva",
      inscriereBaschet.numeRezerva + " " + inscriereBaschet.prenumeRezerva
    );
    inscriereBaschetFormData.append("undeAflat", inscriereBaschet.undeAflat);

    let isValid = true;

    if (!inscriereBaschet.numeCapitan) {
      toastr.error("Nu ati introdus numele căpitanului!");
      isValid = false;
    } else if (!inscriereBaschet.numeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereBaschet.prenumeCapitan) {
      toastr.error("Nu ati introdus prenumele căpitanului!");
      isValid = false;
    } else if (!inscriereBaschet.prenumeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereBaschet.telefonCapitan) {
      toastr.error("Nu ați introdus numărul de telefon al căpitanului!");
      isValid = false;
    } else if (
      !inscriereBaschet.telefonCapitan.match(
        /^(\+4|)?(07[0-9]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/
      )
    ) {
      toastr.error("Ați introdus un format invalid al numărului de telefon!");
      isValid = false;
    }

    if (!inscriereBaschet.emailCapitan) {
      toastr.error("Nu ați introdus emailul căpitanului");
      isValid = false;
    }

    if (
      !inscriereBaschet.emailCapitan.match(
        "^[\x21-\x7E]+@[\x21-\x7E]+.[\x21-\x7E]+$"
      )
    ) {
      toastr.error("E-mailul nu este scris corect!");
      isValid = false;
    }

    if (!inscriereBaschet.studiiCapitan) {
      toastr.error("Nu ati ales un nivel de studii!");
      isValid = false;
    }

    if (!inscriereBaschet.undeAflat) {
      toastr.error("Câmpul de informații trebuie să fie completat!");
      isValid = false;
    } else if (
      inscriereBaschet.undeAflat.length >= 101 ||
      inscriereBaschet.undeAflat.length <= 2
    ) {
      toastr.error(
        "Câmpul de informații trebuie sa conțină între 3 și 100 de caractere!"
      );
      isValid = false;
    } else if (
      !inscriereBaschet.undeAflat.match(/^[a-zA-Z\-ăĂîÎțȚţŢșȘşŞâÂ ]+$/)
    ) {
      toastr.error("Câmpul de informații nu poate conține caractere speciale!");
      isValid = false;
    }

    if (!inscriereBaschet.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereBaschet.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereBaschet.prenumeCoechipier1) {
      toastr.error("Nu ati introdus prenumule primului coechipier!");
      isValid = false;
    } else if (!inscriereBaschet.prenumeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumule primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereBaschet.numeCoechipier2) {
      toastr.error("Nu ați introdus numele pentru al doilea coechipier!");
      isValid = false;
    } else if (!inscriereBaschet.numeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al doilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereBaschet.prenumeCoechipier2) {
      toastr.error("Nu ați introdus prenumele celui de al doilea coechipier!");
      isValid = false;
    } else if (!inscriereBaschet.prenumeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al doilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereBaschet.numeEchipa) {
      toastr.error("Nu ați introdus nume echipei!");
      isValid = false;
    } else if (!inscriereBaschet.numeEchipa.match("[a-zA-Z ]+")) {
      toastr.error("Numele echipei nu trebuie sa conțină caractere speciale!");
      isValid = false;
    }

    if (!inscriereBaschet.regulament) {
      toastr.error("Trebuie să fiți de acord cu termenii și condițiile!");
      isValid = false;
    }

    if (!inscriereBaschet.gdpr) {
      toastr.error(
        "Trebuie să fiți de acord cu regulamentul de prelucrare al datelor personale!"
      );
      isValid = false;
    }

    if (isValid) {
      //transmitted catre back
      axios
        .post(
          "http://localhost:7070/api/baschet/create",
          //   "https://hope.sisc.ro/api/baschet/create",
          inscriereBaschetFormData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
        .then((res) => {
          //console.log(res);
          window.location.replace("http://localhost:7070/succesBaschet");
          //   window.location.replace("https://hope.sisc.ro/succesBaschet");
        })
        .catch((err) => {
          const errValues = Object.values(err.response.data);
          errValues.map((itm) => {
            toastr.error(itm);
          });
        });
    }
};
