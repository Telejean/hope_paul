window.onload = () => {
  let submitBtn = document.getElementById("submit");

  submitBtn.addEventListener("click", (e) => {
    e.preventDefault();

    const inscriereEscaperoom = {
      nume: document.getElementById("nume").value,
      prenume: document.getElementById("prenume").value,
      email: document.getElementById("email").value,
      telefon: document.getElementById("telefon").value,
      studii: document.getElementById("nivelStudii").value,
      undeAflat: document.getElementById("info").value,
      gdpr: document.getElementById("gdpr").checked,
    };

    let inscriereEscaperoomFormData = new FormData();
    inscriereEscaperoomFormData.append(
      "nume",
      inscriereEscaperoom.nume + " " + inscriereEscaperoom.prenume
    );
    inscriereEscaperoomFormData.append("email", inscriereEscaperoom.email);
    inscriereEscaperoomFormData.append("telefon", inscriereEscaperoom.telefon);
    inscriereEscaperoomFormData.append("studii", inscriereEscaperoom.studii);
    inscriereEscaperoomFormData.append(
      "undeAflat",
      inscriereEscaperoom.undeAflat
    );

    let isValid = true;

    if (!inscriereEscaperoom.nume) {
      toastr.error("Nu ati introdus numele");
      isValid = false;
    } else if (
      inscriereEscaperoom.nume.length < 3 ||
      inscriereEscaperoom.nume.length > 30
    ) {
      toastr.error("Numele trebuie sa contina intre 3 si 30 de caractere!");
      isValid = false;
    } else if (!inscriereEscaperoom.nume.match("[a-zA-Z ]+")) {
      toastr.error("Numele nu poate contine spatii sau caractere speciale");
      isValid = false;
    }

    if (!inscriereEscaperoom.prenume) {
      toastr.error("Nu ati introdus prenumele");
      isValid = false;
    } else if (
      inscriereEscaperoom.prenume.length < 3 ||
      inscriereEscaperoom.prenume.length > 30
    ) {
      toastr.error("Prenumele trebuie sa contina intre 3 si 30 de caractere!");
      isValid = false;
    } else if (!inscriereEscaperoom.prenume.match("[a-zA-Z ]+")) {
      toastr.error("Preumele nu poate contine spatii sau caractere speciale");
      isValid = false;
    }

    if (!inscriereEscaperoom.telefon) {
      toastr.error("Nu ati introdus numarul de telefon");
      isValid = false;
    } else if (
      !inscriereEscaperoom.telefon.match(
        /^(\+4|)?(07[0-9]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/
      )
    ) {
      toastr.error("Ati introdus un format invalid al numarului de telefon");
      isValid = false;
    }

    if (!inscriereEscaperoom.email) {
      toastr.error("Nu ati introdus emailul");
      isValid = false;
    }

    if (
      !inscriereEscaperoom.email.match(
        "^[\x21-\x7E]+@[\x21-\x7E]+.[\x21-\x7E]+$"
      )
    ) {
      toastr.error("E-mailul nu este scris corect!");
      isValid = false;
    }

    if (!inscriereEscaperoom.studii) {
      toastr.error("Nu ati ales un nivel de studii");
      isValid = false;
    }

    if (!inscriereEscaperoom.undeAflat) {
      toastr.error("Campul de informatii trebuie sa fie completat");
      isValid = false;
    } else if (
      inscriereEscaperoom.undeAflat.length >= 101 ||
      inscriereEscaperoom.undeAflat.length <= 2
    ) {
      toastr.error(
        "Campul de informatii trebuie sa contina intre 3 si 100 de caractere"
      );
      isValid = false;
    } else if (!inscriereEscaperoom.undeAflat.match("[a-zA-Z ]+")) {
      toastr.error("Campul de informatii nu poate contine caractere speciale");
      isValid = false;
    }

    if (!inscriereEscaperoom.gdpr) {
      toastr.error(
        "Trebuie să fiți de acord cu regulamentul de prelucrare al datelor personale"
      );
      isValid = false;
    }

    if (isValid) {
      //transmitted catre back
      axios
        .post(
          // "http://localhost:8080/api/escape/create",
          "https://hope.sisc.ro/api/escape/create",
          inscriereEscaperoomFormData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
        .then((res) => {
          //console.log(res);
          // window.location.replace("http://localhost:8080/succesEscapeRoom");
          window.location.replace("https://hope.sisc.ro/succesEscapeRoom");
        })
        .catch((err) => {
          const errValues = Object.values(err.response.data);
          errValues.map((itm) => {
            toastr.error(itm);
          });
        });
    }
  });
};
