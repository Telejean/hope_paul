window.onload = () => {
  let submitBtn = document.getElementById("submit");

  submitBtn.addEventListener("click", (e) => {
    e.preventDefault();

    const inscriereFotbal = {
      numeEchipa: document.getElementById("numeEchipa").value,
      numeCapitan: document.getElementById("numeCapitan").value,
      prenumeCapitan: document.getElementById("prenumeCapitan").value,
      emailCapitan: document.getElementById("emailCapitan").value,
      telefonCapitan: document.getElementById("telefonCapitan").value,
      studiiCapitan: document.getElementById("nivelStudii").value,
      numeCoechipier1: document.getElementById("numeCoechipier1").value,
      prenumeCoechipier1: document.getElementById("prenumeCoechipier1").value,
      numeCoechipier2: document.getElementById("numeCoechipier2").value,
      prenumeCoechipier2: document.getElementById("prenumeCoechipier2").value,
      numeCoechipier3: document.getElementById("numeCoechipier3").value,
      prenumeCoechipier3: document.getElementById("prenumeCoechipier3").value,
      numeCoechipier4: document.getElementById("numeCoechipier4").value,
      prenumeCoechipier4: document.getElementById("prenumeCoechipier4").value,
      numeCoechipier5: document.getElementById("numeCoechipier5").value,
      prenumeCoechipier5: document.getElementById("prenumeCoechipier5").value,
      numeRezerva1: document.getElementById("numeRezerva1").value,
      prenumeRezerva1: document.getElementById("prenumeRezerva1").value,
      numeRezerva2: document.getElementById("numeRezerva2").value,
      prenumeRezerva2: document.getElementById("prenumeRezerva2").value,
      undeAflat: document.getElementById("info").value,
      regulament: document.getElementById("regulament").checked,
      gdpr: document.getElementById("gdpr").checked,
    };

    let inscriereFotbalFormData = new FormData();
    inscriereFotbalFormData.append("numeEchipa", inscriereFotbal.numeEchipa);
    inscriereFotbalFormData.append(
      "numeCapitan",
      inscriereFotbal.numeCapitan + " " + inscriereFotbal.prenumeCapitan
    );
    inscriereFotbalFormData.append(
      "emailCapitan",
      inscriereFotbal.emailCapitan
    );
    inscriereFotbalFormData.append(
      "telefonCapitan",
      inscriereFotbal.telefonCapitan
    );
    inscriereFotbalFormData.append(
      "studiiCapitan",
      inscriereFotbal.studiiCapitan
    );
    inscriereFotbalFormData.append(
      "numeCoechipier1",
      inscriereFotbal.numeCoechipier1 + " " + inscriereFotbal.prenumeCoechipier1
    );
    inscriereFotbalFormData.append(
      "numeCoechipier2",
      inscriereFotbal.numeCoechipier2 + " " + inscriereFotbal.prenumeCoechipier2
    );
    inscriereFotbalFormData.append(
      "numeCoechipier3",
      inscriereFotbal.numeCoechipier3 + " " + inscriereFotbal.prenumeCoechipier3
    );
    inscriereFotbalFormData.append(
      "numeCoechipier4",
      inscriereFotbal.numeCoechipier4 + " " + inscriereFotbal.prenumeCoechipier4
    );
    inscriereFotbalFormData.append(
      "numeCoechipier5",
      inscriereFotbal.numeCoechipier5 + " " + inscriereFotbal.prenumeCoechipier5
    );
    inscriereFotbalFormData.append(
      "numeRezerva1",
      inscriereFotbal.numeRezerva1 + " " + inscriereFotbal.prenumeRezerva1
    );
    inscriereFotbalFormData.append(
      "numeRezerva2",
      inscriereFotbal.numeRezerva2 + " " + inscriereFotbal.prenumeRezerva2
    );
    inscriereFotbalFormData.append("undeAflat", inscriereFotbal.undeAflat);

    let isValid = true;

    if (!inscriereFotbal.numeCapitan) {
      toastr.error("Nu ati introdus numele căpitanului!");
      isValid = false;
    } else if (!inscriereFotbal.numeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.prenumeCapitan) {
      toastr.error("Nu ati introdus prenumele căpitanului!");
      isValid = false;
    } else if (!inscriereFotbal.prenumeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.telefonCapitan) {
      toastr.error("Nu ați introdus numărul de telefon al căpitanului!");
      isValid = false;
    } else if (
      !inscriereFotbal.telefonCapitan.match(
        /^(\+4|)?(07[0-9]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/
      )
    ) {
      toastr.error("Ați introdus un format invalid al numărului de telefon!");
      isValid = false;
    }

    if (!inscriereFotbal.emailCapitan) {
      toastr.error("Nu ați introdus emailul căpitanului");
      isValid = false;
    }

    if (
      !inscriereFotbal.emailCapitan.match(
        "^[\x21-\x7E]+@[\x21-\x7E]+.[\x21-\x7E]+$"
      )
    ) {
      toastr.error("E-mailul nu este scris corect!");
      isValid = false;
    }

    if (!inscriereFotbal.studiiCapitan) {
      toastr.error("Nu ati ales un nivel de studii!");
      isValid = false;
    }

    if (!inscriereFotbal.undeAflat) {
      toastr.error("Câmpul de informații trebuie să fie completat!");
      isValid = false;
    } else if (
      inscriereFotbal.undeAflat.length >= 101 ||
      inscriereFotbal.undeAflat.length <= 2
    ) {
      toastr.error(
        "Câmpul de informații trebuie sa conțină între 3 și 100 de caractere!"
      );
      isValid = false;
    } else if (!inscriereFotbal.undeAflat.match("[a-zA-Z ]+")) {
      toastr.error("Câmpul de informații nu poate conține caractere speciale!");
      isValid = false;
    }

    if (!inscriereFotbal.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.prenumeCoechipier1) {
      toastr.error("Nu ati introdus prenumule primului coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.prenumeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumule primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.numeCoechipier2) {
      toastr.error("Nu ați introdus numele pentru al doilea coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.numeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al doilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.prenumeCoechipier2) {
      toastr.error("Nu ați introdus prenumele celui de al doilea coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.prenumeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al doilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.numeCoechipier3) {
      toastr.error("Nu ați introdus numele pentru al treilea coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.numeCoechipier3.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al treilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.prenumeCoechipier3) {
      toastr.error("Nu ați introdus prenumele celui de al treilea coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.prenumeCoechipier3.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al treilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.numeCoechipier4) {
      toastr.error("Nu ați introdus numele pentru al patrulea coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.numeCoechipier4.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al patrulea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.prenumeCoechipier4) {
      toastr.error(
        "Nu ați introdus prenumele celui de al patrulea coechipier!"
      );
      isValid = false;
    } else if (!inscriereFotbal.prenumeCoechipier4.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al patrulea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.numeCoechipier5) {
      toastr.error("Nu ați introdus numele pentru al cincilea coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.numeCoechipier5.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al cincilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.prenumeCoechipier5) {
      toastr.error(
        "Nu ați introdus prenumele celui de al cincilea coechipier!"
      );
      isValid = false;
    } else if (!inscriereFotbal.prenumeCoechipier5.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al cincilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFotbal.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereFotbal.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    // if (!inscriereFotbal.numeRezerva1) {
    //   toastr.error("Nu ati introdus numele primei rezerve!");
    //   isValid = false;
    // } else if (!inscriereFotbal.numeRezerva1.match("[a-zA-Z ]+")) {
    //   toastr.error(
    //     "Numele primei rezerve nu trebuie să conțină caractere speciale!"
    //   );
    //   isValid = false;
    // }

    // if (!inscriereFotbal.prenumeRezerva1) {
    //   toastr.error("Nu ati introdus prenumele primei rezerve!");
    //   isValid = false;
    // } else if (!inscriereFotbal.prenumeRezerva1.match("[a-zA-Z ]+")) {
    //   toastr.error(
    //     "Prenumele primei rezerve nu trebuie să conțină caractere speciale!"
    //   );
    //   isValid = false;
    // }

    // if (!inscriereFotbal.numeRezerva2) {
    //   toastr.error("Nu ati introdus numele celei de a doua rezerve!");
    //   isValid = false;
    // } else if (!inscriereFotbal.numeRezerva2.match("[a-zA-Z ]+")) {
    //   toastr.error(
    //     "Numele celei de a doua rezerve nu trebuie să conțină caractere speciale!"
    //   );
    //   isValid = false;
    // }

    if (!inscriereFotbal.numeEchipa) {
      toastr.error("Nu ați introdus nume echipei!");
      isValid = false;
    } else if (!inscriereFotbal.numeEchipa.match("[a-zA-Z ]+")) {
      toastr.error("Numele echipei nu trebuie sa conțină caractere speciale!");
      isValid = false;
    }

    if (!inscriereFotbal.regulament) {
      toastr.error("Trebuie să fiți de acord cu termenii și condițiile!");
      isValid = false;
    }

    if (!inscriereFotbal.gdpr) {
      toastr.error(
        "Trebuie să fiți de acord cu regulamentul de prelucrare al datelor personale!"
      );
      isValid = false;
    }

    if (isValid) {
      //transmitted catre back
      axios
        .post(
          // "http://localhost:8080/api/fotbal/create",
          "https://hope.sisc.ro/api/fotbal/create",
          inscriereFotbalFormData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
        .then((res) => {
          // console.log(res);
          // window.location.replace("http://localhost:8080/succesFotbal");
          window.location.replace("https://hope.sisc.ro/succesFotbal");
        })
        .catch((err) => {
          const errValues = Object.values(err.response.data);
          errValues.map((itm) => {
            toastr.error(itm);
          });
        });
    }
  });
};
