window.onload = () => {
  let submitBtn = document.getElementById("submit");

  submitBtn.addEventListener("click", (e) => {
    e.preventDefault();

    const inscriereLoL = {
      numeEchipa: document.getElementById("numeEchipa").value,
      numeCapitan: document.getElementById("numeCapitan").value,
      prenumeCapitan: document.getElementById("prenumeCapitan").value,
      emailCapitan: document.getElementById("emailCapitan").value,
      telefonCapitan: document.getElementById("telefonCapitan").value,
      studiiCapitan: document.getElementById("nivelStudii").value,
      numeCoechipier1: document.getElementById("numeCoechipier1").value,
      prenumeCoechipier1: document.getElementById("prenumeCoechipier1").value,
      numeCoechipier2: document.getElementById("numeCoechipier2").value,
      prenumeCoechipier2: document.getElementById("prenumeCoechipier2").value,
      numeCoechipier3: document.getElementById("numeCoechipier3").value,
      prenumeCoechipier3: document.getElementById("prenumeCoechipier3").value,
      numeCoechipier4: document.getElementById("numeCoechipier4").value,
      prenumeCoechipier4: document.getElementById("prenumeCoechipier4").value,
      numeCoechipier5: document.getElementById("numeCoechipier5").value,
      prenumeCoechipier5: document.getElementById("prenumeCoechipier5").value,
      numeRezerva1: document.getElementById("numeRezerva1").value,
      prenumeRezerva1: document.getElementById("prenumeRezerva1").value,
      numeRezerva2: document.getElementById("numeRezerva2").value,
      prenumeRezerva2: document.getElementById("prenumeRezerva2").value,
      undeAflat: document.getElementById("info").value,
      regulament: document.getElementById("regulament").checked,
      gdpr: document.getElementById("gdpr").checked,
    };

    let inscriereLoLFormData = new FormData();
    inscriereLoLFormData.append("numeEchipa", inscriereLoL.numeEchipa);
    inscriereLoLFormData.append(
      "numeCapitan",
      inscriereLoL.numeCapitan + " " + inscriereLoL.prenumeCapitan
    );
    inscriereLoLFormData.append(
      "emailCapitan",
      inscriereLoL.emailCapitan
    );
    inscriereLoLFormData.append(
      "telefonCapitan",
      inscriereLoL.telefonCapitan
    );
    inscriereLoLFormData.append(
      "studiiCapitan",
      inscriereLoL.studiiCapitan
    );
    inscriereLoLFormData.append(
      "numeCoechipier1",
      inscriereLoL.numeCoechipier1 + " " + inscriereLoL.prenumeCoechipier1
    );
    inscriereLoLFormData.append(
      "numeCoechipier2",
      inscriereLoL.numeCoechipier2 + " " + inscriereLoL.prenumeCoechipier2
    );
    inscriereLoLFormData.append(
      "numeCoechipier3",
      inscriereLoL.numeCoechipier3 + " " + inscriereLoL.prenumeCoechipier3
    );
    inscriereLoLFormData.append(
      "numeCoechipier4",
      inscriereLoL.numeCoechipier4 + " " + inscriereLoL.prenumeCoechipier4
    );
    inscriereLoLFormData.append(
      "numeCoechipier5",
      inscriereLoL.numeCoechipier5 + " " + inscriereLoL.prenumeCoechipier5
    );
    inscriereLoLFormData.append(
      "numeRezerva1",
      inscriereLoL.numeRezerva1 + " " + inscriereLoL.prenumeRezerva1
    );
    inscriereLoLFormData.append(
      "numeRezerva2",
      inscriereLoL.numeRezerva2 + " " + inscriereLoL.prenumeRezerva2
    );
    inscriereLoLFormData.append("undeAflat", inscriereLoL.undeAflat);

    let isValid = true;

    if (!inscriereLoL.numeCapitan) {
      toastr.error("Nu ati introdus numele căpitanului!");
      isValid = false;
    } else if (!inscriereLoL.numeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.prenumeCapitan) {
      toastr.error("Nu ati introdus prenumele căpitanului!");
      isValid = false;
    } else if (!inscriereLoL.prenumeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.telefonCapitan) {
      toastr.error("Nu ați introdus numărul de telefon al căpitanului!");
      isValid = false;
    } else if (
      !inscriereLoL.telefonCapitan.match(
        /^(\+4|)?(07[0-9]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/
      )
    ) {
      toastr.error("Ați introdus un format invalid al numărului de telefon!");
      isValid = false;
    }

    if (!inscriereLoL.emailCapitan) {
      toastr.error("Nu ați introdus emailul căpitanului");
      isValid = false;
    }

    if (
      !inscriereLoL.emailCapitan.match(
        "^[\x21-\x7E]+@[\x21-\x7E]+.[\x21-\x7E]+$"
      )
    ) {
      toastr.error("E-mailul nu este scris corect!");
      isValid = false;
    }

    if (!inscriereLoL.studiiCapitan) {
      toastr.error("Nu ati ales un nivel de studii!");
      isValid = false;
    }

    if (!inscriereLoL.undeAflat) {
      toastr.error("Câmpul de informații trebuie să fie completat!");
      isValid = false;
    } else if (
      inscriereLoL.undeAflat.length >= 101 ||
      inscriereLoL.undeAflat.length <= 2
    ) {
      toastr.error(
        "Câmpul de informații trebuie sa conțină între 3 și 100 de caractere!"
      );
      isValid = false;
    } else if (!inscriereLoL.undeAflat.match("[a-zA-Z ]+")) {
      toastr.error("Câmpul de informații nu poate conține caractere speciale!");
      isValid = false;
    }

    if (!inscriereLoL.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereLoL.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.prenumeCoechipier1) {
      toastr.error("Nu ati introdus prenumule primului coechipier!");
      isValid = false;
    } else if (!inscriereLoL.prenumeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumule primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.numeCoechipier2) {
      toastr.error("Nu ați introdus numele pentru al doilea coechipier!");
      isValid = false;
    } else if (!inscriereLoL.numeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al doilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.prenumeCoechipier2) {
      toastr.error("Nu ați introdus prenumele celui de al doilea coechipier!");
      isValid = false;
    } else if (!inscriereLoL.prenumeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al doilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.numeCoechipier3) {
      toastr.error("Nu ați introdus numele pentru al treilea coechipier!");
      isValid = false;
    } else if (!inscriereLoL.numeCoechipier3.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al treilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.prenumeCoechipier3) {
      toastr.error("Nu ați introdus prenumele celui de al treilea coechipier!");
      isValid = false;
    } else if (!inscriereLoL.prenumeCoechipier3.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al treilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.numeCoechipier4) {
      toastr.error("Nu ați introdus numele pentru al patrulea coechipier!");
      isValid = false;
    } else if (!inscriereLoL.numeCoechipier4.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al patrulea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.prenumeCoechipier4) {
      toastr.error(
        "Nu ați introdus prenumele celui de al patrulea coechipier!"
      );
      isValid = false;
    } else if (!inscriereLoL.prenumeCoechipier4.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al patrulea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.numeCoechipier5) {
      toastr.error("Nu ați introdus numele pentru al cincilea coechipier!");
      isValid = false;
    } else if (!inscriereLoL.numeCoechipier5.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al cincilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.prenumeCoechipier5) {
      toastr.error(
        "Nu ați introdus prenumele celui de al cincilea coechipier!"
      );
      isValid = false;
    } else if (!inscriereLoL.prenumeCoechipier5.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al cincilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereLoL.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereLoL.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }


    if (!inscriereLoL.numeEchipa) {
      toastr.error("Nu ați introdus nume echipei!");
      isValid = false;
    } else if (!inscriereLoL.numeEchipa.match("[a-zA-Z ]+")) {
      toastr.error("Numele echipei nu trebuie sa conțină caractere speciale!");
      isValid = false;
    }

    if (!inscriereLoL.regulament) {
      toastr.error("Trebuie să fiți de acord cu termenii și condițiile!");
      isValid = false;
    }

    if (!inscriereLoL.gdpr) {
      toastr.error(
        "Trebuie să fiți de acord cu regulamentul de prelucrare al datelor personale!"
      );
      isValid = false;
    }

    if (isValid) {
      //transmitted catre back
      axios
        .post(
          // "http://localhost:8080/api/LoL/create",
          "https://hope.sisc.ro/api/LoL/create",
          inscriereLoLFormData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
        .then((res) => {
          // console.log(res);
          // window.location.replace("http://localhost:8080/succesLoL");
          window.location.replace("https://hope.sisc.ro/succesLoL");
        })
        .catch((err) => {
          const errValues = Object.values(err.response.data);
          errValues.map((itm) => {
            toastr.error(itm);
          });
        });
    }
  });
};
