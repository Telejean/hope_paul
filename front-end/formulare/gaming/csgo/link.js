window.onload = () => {
  let submitBtn = document.getElementById("submit");

  submitBtn.addEventListener("click", (e) => {
    e.preventDefault();

    const inscriereCSGO = {
      numeEchipa: document.getElementById("numeEchipa").value,
      numeCapitan: document.getElementById("numeCapitan").value,
      prenumeCapitan: document.getElementById("prenumeCapitan").value,
      emailCapitan: document.getElementById("emailCapitan").value,
      telefonCapitan: document.getElementById("telefonCapitan").value,
      studiiCapitan: document.getElementById("nivelStudii").value,
      numeCoechipier1: document.getElementById("numeCoechipier1").value,
      prenumeCoechipier1: document.getElementById("prenumeCoechipier1").value,
      numeCoechipier2: document.getElementById("numeCoechipier2").value,
      prenumeCoechipier2: document.getElementById("prenumeCoechipier2").value,
      numeCoechipier3: document.getElementById("numeCoechipier3").value,
      prenumeCoechipier3: document.getElementById("prenumeCoechipier3").value,
      numeCoechipier4: document.getElementById("numeCoechipier4").value,
      prenumeCoechipier4: document.getElementById("prenumeCoechipier4").value,
      numeCoechipier5: document.getElementById("numeCoechipier5").value,
      prenumeCoechipier5: document.getElementById("prenumeCoechipier5").value,
      numeRezerva1: document.getElementById("numeRezerva1").value,
      prenumeRezerva1: document.getElementById("prenumeRezerva1").value,
      numeRezerva2: document.getElementById("numeRezerva2").value,
      prenumeRezerva2: document.getElementById("prenumeRezerva2").value,
      undeAflat: document.getElementById("info").value,
      regulament: document.getElementById("regulament").checked,
      gdpr: document.getElementById("gdpr").checked,
    };

    let inscriereCSGOFormData = new FormData();
    inscriereCSGOFormData.append("numeEchipa", inscriereCSGO.numeEchipa);
    inscriereCSGOFormData.append(
      "numeCapitan",
      inscriereCSGO.numeCapitan + " " + inscriereCSGO.prenumeCapitan
    );
    inscriereCSGOFormData.append(
      "emailCapitan",
      inscriereCSGO.emailCapitan
    );
    inscriereCSGOFormData.append(
      "telefonCapitan",
      inscriereCSGO.telefonCapitan
    );
    inscriereCSGOFormData.append(
      "studiiCapitan",
      inscriereCSGO.studiiCapitan
    );
    inscriereCSGOFormData.append(
      "numeCoechipier1",
      inscriereCSGO.numeCoechipier1 + " " + inscriereCSGO.prenumeCoechipier1
    );
    inscriereCSGOFormData.append(
      "numeCoechipier2",
      inscriereCSGO.numeCoechipier2 + " " + inscriereCSGO.prenumeCoechipier2
    );
    inscriereCSGOFormData.append(
      "numeCoechipier3",
      inscriereCSGO.numeCoechipier3 + " " + inscriereCSGO.prenumeCoechipier3
    );
    inscriereCSGOFormData.append(
      "numeCoechipier4",
      inscriereCSGO.numeCoechipier4 + " " + inscriereCSGO.prenumeCoechipier4
    );
    inscriereCSGOFormData.append(
      "numeCoechipier5",
      inscriereCSGO.numeCoechipier5 + " " + inscriereCSGO.prenumeCoechipier5
    );
    inscriereCSGOFormData.append(
      "numeRezerva1",
      inscriereCSGO.numeRezerva1 + " " + inscriereCSGO.prenumeRezerva1
    );
    inscriereCSGOFormData.append(
      "numeRezerva2",
      inscriereCSGO.numeRezerva2 + " " + inscriereCSGO.prenumeRezerva2
    );
    inscriereCSGOFormData.append("undeAflat", inscriereCSGO.undeAflat);

    let isValid = true;

    if (!inscriereCSGO.numeCapitan) {
      toastr.error("Nu ati introdus numele căpitanului!");
      isValid = false;
    } else if (!inscriereCSGO.numeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.prenumeCapitan) {
      toastr.error("Nu ati introdus prenumele căpitanului!");
      isValid = false;
    } else if (!inscriereCSGO.prenumeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.telefonCapitan) {
      toastr.error("Nu ați introdus numărul de telefon al căpitanului!");
      isValid = false;
    } else if (
      !inscriereCSGO.telefonCapitan.match(
        /^(\+4|)?(07[0-9]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/
      )
    ) {
      toastr.error("Ați introdus un format invalid al numărului de telefon!");
      isValid = false;
    }

    if (!inscriereCSGO.emailCapitan) {
      toastr.error("Nu ați introdus emailul căpitanului");
      isValid = false;
    }

    if (
      !inscriereCSGO.emailCapitan.match(
        "^[\x21-\x7E]+@[\x21-\x7E]+.[\x21-\x7E]+$"
      )
    ) {
      toastr.error("E-mailul nu este scris corect!");
      isValid = false;
    }

    if (!inscriereCSGO.studiiCapitan) {
      toastr.error("Nu ati ales un nivel de studii!");
      isValid = false;
    }

    if (!inscriereCSGO.undeAflat) {
      toastr.error("Câmpul de informații trebuie să fie completat!");
      isValid = false;
    } else if (
      inscriereCSGO.undeAflat.length >= 101 ||
      inscriereCSGO.undeAflat.length <= 2
    ) {
      toastr.error(
        "Câmpul de informații trebuie sa conțină între 3 și 100 de caractere!"
      );
      isValid = false;
    } else if (!inscriereCSGO.undeAflat.match("[a-zA-Z ]+")) {
      toastr.error("Câmpul de informații nu poate conține caractere speciale!");
      isValid = false;
    }

    if (!inscriereCSGO.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.prenumeCoechipier1) {
      toastr.error("Nu ati introdus prenumule primului coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.prenumeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumule primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.numeCoechipier2) {
      toastr.error("Nu ați introdus numele pentru al doilea coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.numeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al doilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.prenumeCoechipier2) {
      toastr.error("Nu ați introdus prenumele celui de al doilea coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.prenumeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al doilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.numeCoechipier3) {
      toastr.error("Nu ați introdus numele pentru al treilea coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.numeCoechipier3.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al treilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.prenumeCoechipier3) {
      toastr.error("Nu ați introdus prenumele celui de al treilea coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.prenumeCoechipier3.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al treilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.numeCoechipier4) {
      toastr.error("Nu ați introdus numele pentru al patrulea coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.numeCoechipier4.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al patrulea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.prenumeCoechipier4) {
      toastr.error(
        "Nu ați introdus prenumele celui de al patrulea coechipier!"
      );
      isValid = false;
    } else if (!inscriereCSGO.prenumeCoechipier4.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al patrulea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.numeCoechipier5) {
      toastr.error("Nu ați introdus numele pentru al cincilea coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.numeCoechipier5.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al cincilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.prenumeCoechipier5) {
      toastr.error(
        "Nu ați introdus prenumele celui de al cincilea coechipier!"
      );
      isValid = false;
    } else if (!inscriereCSGO.prenumeCoechipier5.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al cincilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereCSGO.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereCSGO.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }


    if (!inscriereCSGO.numeEchipa) {
      toastr.error("Nu ați introdus nume echipei!");
      isValid = false;
    } else if (!inscriereCSGO.numeEchipa.match("[a-zA-Z ]+")) {
      toastr.error("Numele echipei nu trebuie sa conțină caractere speciale!");
      isValid = false;
    }

    if (!inscriereCSGO.regulament) {
      toastr.error("Trebuie să fiți de acord cu termenii și condițiile!");
      isValid = false;
    }

    if (!inscriereCSGO.gdpr) {
      toastr.error(
        "Trebuie să fiți de acord cu regulamentul de prelucrare al datelor personale!"
      );
      isValid = false;
    }

    if (isValid) {
      //transmitted catre back
      axios
        .post(
          // "http://localhost:8080/api/CSGO/create",
          "https://hope.sisc.ro/api/CSGO/create",
          inscriereCSGOFormData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
        .then((res) => {
          // console.log(res);
          // window.location.replace("http://localhost:8080/succesCSGO");
          window.location.replace("https://hope.sisc.ro/succesCSGO");
        })
        .catch((err) => {
          const errValues = Object.values(err.response.data);
          errValues.map((itm) => {
            toastr.error(itm);
          });
        });
    }
  });
};
