window.onload = () => {
  let submitBtn = document.getElementById("submit");

  submitBtn.addEventListener("click", (e) => {
    e.preventDefault();

    const inscriereFIFA = {
      numeEchipa: document.getElementById("numeEchipa").value,
      numeCapitan: document.getElementById("numeCapitan").value,
      prenumeCapitan: document.getElementById("prenumeCapitan").value,
      emailCapitan: document.getElementById("emailCapitan").value,
      telefonCapitan: document.getElementById("telefonCapitan").value,
      studiiCapitan: document.getElementById("nivelStudii").value,
      numeCoechipier1: document.getElementById("numeCoechipier1").value,
      prenumeCoechipier1: document.getElementById("prenumeCoechipier1").value,
      numeCoechipier2: document.getElementById("numeCoechipier2").value,
      prenumeCoechipier2: document.getElementById("prenumeCoechipier2").value,
      numeCoechipier3: document.getElementById("numeCoechipier3").value,
      prenumeCoechipier3: document.getElementById("prenumeCoechipier3").value,
      numeCoechipier4: document.getElementById("numeCoechipier4").value,
      prenumeCoechipier4: document.getElementById("prenumeCoechipier4").value,
      numeCoechipier5: document.getElementById("numeCoechipier5").value,
      prenumeCoechipier5: document.getElementById("prenumeCoechipier5").value,
      numeRezerva1: document.getElementById("numeRezerva1").value,
      prenumeRezerva1: document.getElementById("prenumeRezerva1").value,
      numeRezerva2: document.getElementById("numeRezerva2").value,
      prenumeRezerva2: document.getElementById("prenumeRezerva2").value,
      undeAflat: document.getElementById("info").value,
      regulament: document.getElementById("regulament").checked,
      gdpr: document.getElementById("gdpr").checked,
    };

    let inscriereFIFAFormData = new FormData();
    inscriereFIFAFormData.append("numeEchipa", inscriereFIFA.numeEchipa);
    inscriereFIFAFormData.append(
      "numeCapitan",
      inscriereFIFA.numeCapitan + " " + inscriereFIFA.prenumeCapitan
    );
    inscriereFIFAFormData.append(
      "emailCapitan",
      inscriereFIFA.emailCapitan
    );
    inscriereFIFAFormData.append(
      "telefonCapitan",
      inscriereFIFA.telefonCapitan
    );
    inscriereFIFAFormData.append(
      "studiiCapitan",
      inscriereFIFA.studiiCapitan
    );
    inscriereFIFAFormData.append(
      "numeCoechipier1",
      inscriereFIFA.numeCoechipier1 + " " + inscriereFIFA.prenumeCoechipier1
    );
    inscriereFIFAFormData.append(
      "numeCoechipier2",
      inscriereFIFA.numeCoechipier2 + " " + inscriereFIFA.prenumeCoechipier2
    );
    inscriereFIFAFormData.append(
      "numeCoechipier3",
      inscriereFIFA.numeCoechipier3 + " " + inscriereFIFA.prenumeCoechipier3
    );
    inscriereFIFAFormData.append(
      "numeCoechipier4",
      inscriereFIFA.numeCoechipier4 + " " + inscriereFIFA.prenumeCoechipier4
    );
    inscriereFIFAFormData.append(
      "numeCoechipier5",
      inscriereFIFA.numeCoechipier5 + " " + inscriereFIFA.prenumeCoechipier5
    );
    inscriereFIFAFormData.append(
      "numeRezerva1",
      inscriereFIFA.numeRezerva1 + " " + inscriereFIFA.prenumeRezerva1
    );
    inscriereFIFAFormData.append(
      "numeRezerva2",
      inscriereFIFA.numeRezerva2 + " " + inscriereFIFA.prenumeRezerva2
    );
    inscriereFIFAFormData.append("undeAflat", inscriereFIFA.undeAflat);

    let isValid = true;

    if (!inscriereFIFA.numeCapitan) {
      toastr.error("Nu ati introdus numele căpitanului!");
      isValid = false;
    } else if (!inscriereFIFA.numeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.prenumeCapitan) {
      toastr.error("Nu ati introdus prenumele căpitanului!");
      isValid = false;
    } else if (!inscriereFIFA.prenumeCapitan.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele căpitanului nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.telefonCapitan) {
      toastr.error("Nu ați introdus numărul de telefon al căpitanului!");
      isValid = false;
    } else if (
      !inscriereFIFA.telefonCapitan.match(
        /^(\+4|)?(07[0-9]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/
      )
    ) {
      toastr.error("Ați introdus un format invalid al numărului de telefon!");
      isValid = false;
    }

    if (!inscriereFIFA.emailCapitan) {
      toastr.error("Nu ați introdus emailul căpitanului");
      isValid = false;
    }

    if (
      !inscriereFIFA.emailCapitan.match(
        "^[\x21-\x7E]+@[\x21-\x7E]+.[\x21-\x7E]+$"
      )
    ) {
      toastr.error("E-mailul nu este scris corect!");
      isValid = false;
    }

    if (!inscriereFIFA.studiiCapitan) {
      toastr.error("Nu ati ales un nivel de studii!");
      isValid = false;
    }

    if (!inscriereFIFA.undeAflat) {
      toastr.error("Câmpul de informații trebuie să fie completat!");
      isValid = false;
    } else if (
      inscriereFIFA.undeAflat.length >= 101 ||
      inscriereFIFA.undeAflat.length <= 2
    ) {
      toastr.error(
        "Câmpul de informații trebuie sa conțină între 3 și 100 de caractere!"
      );
      isValid = false;
    } else if (!inscriereFIFA.undeAflat.match("[a-zA-Z ]+")) {
      toastr.error("Câmpul de informații nu poate conține caractere speciale!");
      isValid = false;
    }

    if (!inscriereFIFA.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.prenumeCoechipier1) {
      toastr.error("Nu ati introdus prenumule primului coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.prenumeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumule primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.numeCoechipier2) {
      toastr.error("Nu ați introdus numele pentru al doilea coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.numeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al doilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.prenumeCoechipier2) {
      toastr.error("Nu ați introdus prenumele celui de al doilea coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.prenumeCoechipier2.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al doilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.numeCoechipier3) {
      toastr.error("Nu ați introdus numele pentru al treilea coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.numeCoechipier3.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al treilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.prenumeCoechipier3) {
      toastr.error("Nu ați introdus prenumele celui de al treilea coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.prenumeCoechipier3.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al treilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.numeCoechipier4) {
      toastr.error("Nu ați introdus numele pentru al patrulea coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.numeCoechipier4.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al patrulea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.prenumeCoechipier4) {
      toastr.error(
        "Nu ați introdus prenumele celui de al patrulea coechipier!"
      );
      isValid = false;
    } else if (!inscriereFIFA.prenumeCoechipier4.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al patrulea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.numeCoechipier5) {
      toastr.error("Nu ați introdus numele pentru al cincilea coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.numeCoechipier5.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele celui de al cincilea coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.prenumeCoechipier5) {
      toastr.error(
        "Nu ați introdus prenumele celui de al cincilea coechipier!"
      );
      isValid = false;
    } else if (!inscriereFIFA.prenumeCoechipier5.match("[a-zA-Z ]+")) {
      toastr.error(
        "Prenumele celui de al cincilea coechipier nu trebuie sa conțină caractere speciale!"
      );
      isValid = false;
    }

    if (!inscriereFIFA.numeCoechipier1) {
      toastr.error("Nu ați introdus numele primului coechipier!");
      isValid = false;
    } else if (!inscriereFIFA.numeCoechipier1.match("[a-zA-Z ]+")) {
      toastr.error(
        "Numele primului coechipier nu trebuie să conțină caractere speciale!"
      );
      isValid = false;
    }


    if (!inscriereFIFA.numeEchipa) {
      toastr.error("Nu ați introdus nume echipei!");
      isValid = false;
    } else if (!inscriereFIFA.numeEchipa.match("[a-zA-Z ]+")) {
      toastr.error("Numele echipei nu trebuie sa conțină caractere speciale!");
      isValid = false;
    }

    if (!inscriereFIFA.regulament) {
      toastr.error("Trebuie să fiți de acord cu termenii și condițiile!");
      isValid = false;
    }

    if (!inscriereFIFA.gdpr) {
      toastr.error(
        "Trebuie să fiți de acord cu regulamentul de prelucrare al datelor personale!"
      );
      isValid = false;
    }

    if (isValid) {
      //transmitted catre back
      axios
        .post(
          // "http://localhost:8080/api/FIFA/create",
          "https://hope.sisc.ro/api/FIFA/create",
          inscriereFIFAFormData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
        .then((res) => {
          // console.log(res);
          // window.location.replace("http://localhost:8080/succesFIFA");
          window.location.replace("https://hope.sisc.ro/succesFIFA");
        })
        .catch((err) => {
          const errValues = Object.values(err.response.data);
          errValues.map((itm) => {
            toastr.error(itm);
          });
        });
    }
  });
};
