window.onload = () => {
    let submitBtn = document.getElementById("submit");

    submitBtn.addEventListener("click", (e) => {
        e.preventDefault();

        const inscriereLasertag = {
            nume: document.getElementById("nume").value,
            prenume: document.getElementById("prenume").value,
            email: document.getElementById("email").value,
            telefon: document.getElementById("telefon").value,
            studii: document.getElementById("nivelStudii").value,
            undeAflat: document.getElementById("info").value,
            gdpr: document.getElementById("gdpr").checked,
        };

        let inscriereLasertagFormData = new FormData();
        inscriereLasertagFormData.append("nume", inscriereLasertag.nume + " " + inscriereLasertag.prenume);
        inscriereLasertagFormData.append("email", inscriereLasertag.email);
        inscriereLasertagFormData.append("telefon", inscriereLasertag.telefon);
        inscriereLasertagFormData.append("studii", inscriereLasertag.studii);
        inscriereLasertagFormData.append("undeAflat", inscriereLasertag.undeAflat);

        let isValid = true;

        if (!inscriereLasertag.nume) {
            toastr.error("Nu ati introdus numele");
            isValid = false;
        } else if (inscriereLasertag.nume.length < 3 || inscriereLasertag.nume.length > 30) {
            toastr.error("Numele trebuie sa contina intre 3 si 30 de caractere!");
            isValid = false;
        } else if (!inscriereLasertag.nume.match(/^[a-zA-Z\-ăĂîÎțȚţŢșȘşŞâÂ]+$/)) {
            toastr.error("Numele nu poate contine spatii sau caractere speciale");
            isValid = false;
        }

        if (!inscriereLasertag.prenume) {
            toastr.error("Nu ati introdus prenumele");
            isValid = false;
        } else if (inscriereLasertag.prenume.length < 3 || inscriereLasertag.prenume.length > 30) {
            toastr.error("Prenumele trebuie sa contina intre 3 si 30 de caractere!");
            isValid = false;
        } else if (!inscriereLasertag.prenume.match(/^[a-zA-Z\-ăĂîÎțȚţŢșȘşŞâÂ]+$/)) {
            toastr.error("Preumele nu poate contine spatii sau caractere speciale");
            isValid = false;
        }

    if (!inscriereLasertag.telefon) {
      toastr.error("Nu ati introdus numarul de telefon");
      isValid = false;
    } else if (
      !inscriereLasertag.telefon.match(
        /^(\+4|)?(07[0-9]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/
      )
    ) {
      toastr.error("Ati introdus un format invalid al numarului de telefon");
      isValid = false;
    }

        if (!inscriereLasertag.email) {
            toastr.error("Nu ati introdus emailul");
            isValid = false;
        }

        if (!inscriereLasertag.email.match("^[\x21-\x7E]+@[\x21-\x7E]+.[\x21-\x7E]+$")) {
            toastr.error("E-mailul nu este scris corect!");
            isValid = false;
        }

        if (!inscriereLasertag.studii) {
            toastr.error("Nu ati introdus facultatea");
            isValid = false;
        }

        if (!inscriereLasertag.undeAflat) {
            toastr.error("Campul de informatii trebuie sa fie completat");
            isValid = false;
        } else if (inscriereLasertag.undeAflat.length >= 101 || inscriereLasertag.undeAflat.length <= 2) {
            toastr.error("Campul de informatii trebuie sa contina intre 3 si 100 de caractere");
            isValid = false;
        } else if (!inscriereLasertag.undeAflat.match(/^[a-zA-Z\-ăĂîÎțȚţŢșȘşŞâÂ ]+$/)) {
            toastr.error("Campul de informatii nu poate contine caractere speciale");
            isValid = false;
        }

        if (!inscriereLasertag.gdpr) {
            toastr.error("Trebuie să fiți de acord cu regulamentul de prelucrare al datelor personale");
            isValid = false;
        }

        if (isValid) {
            console.log(inscriereLasertag);
            //transmitted catre back
            axios
                .post(
                    // "http://localhost:8080/api/lasertag/create",
                    "https://hope.sisc.ro/api/lasertag/create",
                    inscriereLasertagFormData,
                    {
                        headers: {
                            "Content-Type": "multipart/form-data",
                        },
                    }
                )
                .then((res) => {
                    // console.log(res);
                    // window.location.replace("http://localhost:8080/succesLaserTag");
                    window.location.replace("https://hope.sisc.ro/succesLaserTag");
                })
                .catch((err) => {
                    const errValues = Object.values(err.response.data);
                    errValues.map((itm) => {
                        toastr.error(itm);
                    });
                });
        }
    });
};
