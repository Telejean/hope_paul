const navList = document.querySelector(".nav-list");
const hamburgerMenuBtn = document.querySelector(".hamburger-menu");
const closeMenuBtn = document.querySelector(".close-menu");

hamburgerMenuBtn.addEventListener("click", () => {
    navList.classList.add("active");
});

closeMenuBtn.addEventListener("click", () => {
    navList.classList.remove("active");
});
