// Get the modal
let modal1 = document.getElementById("myModal1");
let modal2 = document.getElementById("myModal2");
let modal3 = document.getElementById("myModal3");
let modal4 = document.getElementById("myModal4");
let modal5 = document.getElementById("myModal5");
let modal6 = document.getElementById("myModal6");

// Get the button that opens the modal
let img1 = document.getElementById("openModal1");
let img2 = document.getElementById("openModal2");
let img3 = document.getElementById("openModal3");
let img4 = document.getElementById("openModal4");
let img5 = document.getElementById("openModal5");
let img6 = document.getElementById("openModal6");

// Get the <span> element that closes the modal
let span1 = document.getElementsByClassName("close")[0];
let span2 = document.getElementsByClassName("close")[1];
let span3 = document.getElementsByClassName("close")[2];
let span4 = document.getElementsByClassName("close")[3];
let span5 = document.getElementsByClassName("close")[4];
let span6 = document.getElementsByClassName("close")[5];
console.log(span1);
// When the user clicks on the button, open the modal
img1.addEventListener("click", () => {
  modal1.style.display = "block";
});

img2.addEventListener("click", () => {
  modal2.style.display = "block";
});

img3.addEventListener("click", () => {
  modal3.style.display = "block";
});

img4.addEventListener("click", () => {
  modal4.style.display = "block";
});
img5.addEventListener("click", () => {
  modal5.style.display = "block";
});
img6.addEventListener("click", () => {
  modal6.style.display = "block";
});

// When the user clicks on <span> (x), close the modal
span1.addEventListener("click", () => {
  modal1.style.display = "none";
});
span2.addEventListener("click", () => {
  modal2.style.display = "none";
});
span3.addEventListener("click", () => {
  modal3.style.display = "none";
});
span4.addEventListener("click", () => {
  modal4.style.display = "none";
});
span5.addEventListener("click", () => {
  modal5.style.display = "none";
});
span6.addEventListener("click", () => {
  modal6.style.display = "none";
});

// When the user clicks anywhere outside of the modal, close it
window.addEventListener("click", (e) => {
  if (e.target == modal1) {
    modal1.style.display = "none";
  }
});
window.addEventListener("click", (e) => {
  if (e.target == modal2) {
    modal2.style.display = "none";
  }
});
window.addEventListener("click", (e) => {
  if (e.target == modal3) {
    modal3.style.display = "none";
  }
});
window.addEventListener("click", (e) => {
  if (e.target == modal4) {
    modal4.style.display = "none";
  }
});
window.addEventListener("click", (e) => {
  if (e.target == modal5) {
    modal5.style.display = "none";
  }
});
window.addEventListener("click", (e) => {
  if (e.target == modal6) {
    modal6.style.display = "none";
  }
});
