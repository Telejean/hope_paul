/* -----------------------------------------------
/* How to use? : Check the GitHub README
/* ----------------------------------------------- */

/* To load a config file (particles.json) you need to host this demo (MAMP/WAMP/local)... */
/*
particlesJS.load('particles-js', 'particles.json', function() {
  console.log('particles.js loaded - callback');
});
*/

/* Otherwise just put the config content (json): */

particlesJS(
    "particles-js",

    {
        particles: {
            number: {
                value: 80,
                density: {
                    enable: true,
                    value_area: 800,
                },
            },
            color: {
                value: "#0F849D",
            },
            shape: {
                type: "circle",
            },
            opacity: {
                value: 0.7,
                random: false,
                anim: {
                    enable: false,
                },
            },
            size: {
                value: 5,
                random: true,
                anim: {
                    enable: false,
                },
            },
            line_linked: {
                enable: false,
            },
            move: {
                enable: true,
                speed: 3,
                direction: "bottom-right",
                random: false,
                straight: false,
                out_mode: "out",
                attract: {
                    enable: false,
                    rotateX: 600,
                    rotateY: 1200,
                },
            },
        },
        interactivity: {
            detect_on: "window",
            events: {
                onhover: {
                    enable: true,
                    mode: "bubble",
                },
                onclick: {
                    enable: true,
                    mode: "repulse",
                },
                resize: true,
            },
            modes: {
                bubble: {
                    distance: 100,
                    size: 6,
                    duration: 0.5,
                    opacity: 8,
                    speed: 5,
                },
                repulse: {
                    distance: 200,
                },
            },
        },
    }
);
