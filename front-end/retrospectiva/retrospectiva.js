const years = document.querySelectorAll(".accordion__item");
years[0].classList.add("active");
years.forEach((section) => {
    section.addEventListener("click", () => {
        years.forEach((section) => {
            section.classList.remove("active");
        });
        section.classList.add("active");
    });
});
window.onload = function () {
  // Get the modal
  let modal = [];
  for (let i = 0; i < 8; i++) {
      modal[i] = document.getElementById(`Modal${i + 1}`);
  }

  // Get the button that opens the modal
  let btn = [];
  for (let i = 0; i < 8; i++) {
      btn[i] = document.getElementById(`btn${i + 1}`);
  }

  let span = [];
  for (let i = 0; i < 8; i++) {
      span[i] = document.getElementsByClassName("close");
  }

  const setDisplayBlock = (i) => {
      modal[i].style.display = "block";
      body.style.overflow = "hidden";
  };

  const setDisplayNone = (i) => {
      modal[i].style.display = "none";
      body.style.overflow = "auto";
  };
  for (let i = 0; i < 8; i++) {
      btn[i].onclick = function () {
          setDisplayBlock(i);
      };
  }

  for (let i = 0; i < 8; i++) {
      span[i].onclick = function () {
          setDisplayNone(i);
          console.log("close it");
      };
  }

  // When the user clicks on <span> (x), close the modal

  document.addEventListener(
      "click",
      (event) => {
          if (
              event.target.matches(".close") ||
              (!event.target.closest(".modal-content") &&
                  event.target.closest("modal-retrospectiva"))
          ) {
              console.log("close it");
              for (let i = 0; i < 8; i++) {
                  modal[i].style.display = "none";
              }
          }
      },
      false
  );
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function (event) {
      console.log(modal);
      for (i = 0; i < 8; i++) {
              if (event.target == modal[i]) {
              modal[i].style.display = "none";
              body.style.overflow = "auto";
            }
          }
  };
};
